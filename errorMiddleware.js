const isArray = require('lodash/isArray')
const log = require('./log')

module.exports = (error, req, res, next) => {
  if (error) {
    const {status, errors} = error
    if (status === 400 && isArray(errors)) {
      // assume open api validation error
      res.status(400).json({
        message: 'Open API validation failed',
        errors
      })
    } else {
      log.error(error)
      res.status(500).json(error)
    }
  } else {
    next()
  }
}
