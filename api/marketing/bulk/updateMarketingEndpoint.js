const updateMarketing = require('./updateMarketing')
const ref = require('../../docs/definitions/ref')
const errors = require('../../docs/errors')
const createEndpoint = require('../../createEndpoint')

const update = async (doc) => {
  if (!doc) {
    return {
      status: 400,
      message: `required property 'GUID' missing from deal marketing document`
    }
  } else {
    return updateMarketing(doc)
  }
}

const updateMarketingEndpoint = async ({body}, res) => {
  const {status, message, count} = await update(body)
  if (status) {
    res.status(status).json({message})
  } else {
    res.json({count})
  }
}

const apiDoc = {
  summary: 'Creates / Update a new marketing deal',
  operationId: 'updateMarketing',
  parameters: [
    {
      name: 'body',
      in: 'body',
      required: true,
      schema: {
        type: 'array',
        items: ref('Marketing')
      }
    }
  ],
  responses: {
    200: {
      description: 'Marketing was successfully created or updated',
      schema: ref('Marketing')
    },
    400: errors.create('JSON body was not a valid Deal'),
    401: errors.noCurrentUser,
    403: errors.dealNotAuthorized,
    500: errors.unexpected
  }
}

module.exports = createEndpoint(updateMarketingEndpoint, apiDoc)
