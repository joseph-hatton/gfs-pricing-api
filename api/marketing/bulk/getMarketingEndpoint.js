const getMarketing = require('./getMarketing')
const errors = require('../../docs/errors')
const ref = require('../../docs/definitions/ref')
const createEndpoint = require('../../createEndpoint')

const convertDbRowToResponse = (dbRow) => ({
  ...dbRow.doc,
  id: dbRow.doc.id
})

const getMarketingEndpoint = async (req, res) => {
  const dbRows = await getMarketing()
  res.json(dbRows.map(convertDbRowToResponse))
}

const apiDoc = {
  summary: 'Responds with all marketing sharepoint deals',
  operationId: 'getMarketing',
  parameters: [],
  responses: {
    200: {
      description: 'an array of marketing sharepoint deals',
      schema: {
        type: 'array',
        items: ref('Marketing')
      }
    },
    500: errors.unexpected
  }
}

module.exports = createEndpoint(getMarketingEndpoint, apiDoc)
