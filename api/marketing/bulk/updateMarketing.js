require('require-sql')
const {pool} = require('../../db')
const insertMarketingSql = require('./insert-marketing.sql')
const deleteMarketingSql = require('./delete-marketing.sql')
const log = require('../../../log')
const hasEntitlement = require('../../auth/hasEntitlement')
const {UPDATE_MARKETING} = require('../../auth')
// const getMarketingByGuid = require('./getDealByDealName')

const saveMarketing = async (client, marketingDeals) => {
  await client.query(deleteMarketingSql)
  log.info('deleting previus marketing deal')
  for (const marketingDeal of marketingDeals) {
    log.info({marketingId: marketingDeal.id}, 'saving marketing deal')
    await client.query(insertMarketingSql, [marketingDeal.id, marketingDeal, 'SYSTEM'])
  }
}

const save = async (marketingDeals) => {
  // const existingMarketingDeal = await getMarketingById(deal.id)

  const client = await pool.connect()
  try {
    await client.query('BEGIN')
    log.info('inserting marketing deals')
    await saveMarketing(client, marketingDeals)
    await client.query('COMMIT')
    log.info('successfully saved marketing deals')
    return { count: marketingDeals.length }
  } catch (err) {
    log.error({err}, 'error saving marketing deals')
    await client.query('ROLLBACK')
    throw err
  } finally {
    client.release()
  }
}

module.exports = async (marketingDeals) => {
  const canCreate = hasEntitlement(UPDATE_MARKETING)
  if (canCreate) {
    const {count} = await save(marketingDeals)
    return {count}
  } else {
    return {
      status: 403,
      message: `System is not authorized to create a deals`
    }
  }
}
