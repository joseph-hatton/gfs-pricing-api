const {query} = require('../../db')
require('require-sql')
const sql = require('./get-marketing.sql')

module.exports = async () => (await query(sql)).rows
