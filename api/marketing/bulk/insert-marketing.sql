insert into sch_pricing.marketing (
  id,
  doc,
  last_modified_by
) values (
  $1,
  $2,
  $3
)
returning $1