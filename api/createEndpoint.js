module.exports = (fn, apiDoc) => {
  const endpoint = async (req, res, next) => {
    try {
      await fn(req, res)
    } catch (error) {
      next(error)
    }
  }
  endpoint.apiDoc = apiDoc
  return endpoint
}
