const log = require('../../log')
const getDeals = require('../deals/getDeals')

module.exports = async (req, res) => {
  try {
    await getDeals()
    res.json({ok: true})
  } catch (error) {
    log.error(error)
    res.status(500).json({message: `health check failed: ${error.message}`})
  }
}
