const {env} = process

module.exports = {
  port: parseInt(env.PORT || 3006),
  uploadFilePath: `${__dirname}/uploaded-files`,
  reportingApi: env.API_REPORTING || 'https://gfs-reporting-api-dev.rgare.net/api/v1/reports',
  db: {
    host: env.POSTGRES_HOST || 'localhost',
    port: env.POSTGRES_PORT || 5432,
    user: env.POSTGRES_USER || 'app_pricing',
    password: env.POSTGRES_PASSWORD || 'abc123',
    database: env.POSTGRES_DATABASE || 'stlpricing'
  },
  uploads: {
    scalar: {
      tabName: 'UploadScalar',
      columnsToIgnore: ['revno', 'createddate', 'userid'],
      // companyColumn: 'A',
      // accountingBasisColumn: 'B',
      fieldNameColumn: 'B',
      valueColumn: 'D'
    }
  }
}
