const create = description => ({
  description,
  schema: {
    properties: {
      message: {type: 'string'}
    }
  }
})

module.exports = {
  unexpected: create('Unexpected Error'),
  userNotFound: create('there must be a current user to create a deal'),
  dealNotFound: create('A deal with the given uuid was not found'),
  dealNotAuthorized: create('User is not authorized to create/update/delete deal'),
  noCurrentUser: create('No user credentials were provided'),
  create
}
