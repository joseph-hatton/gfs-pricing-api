const accountingBases = require('../../refs/accountingBases')

module.exports = {
  type: 'string',
  enum: accountingBases
}
