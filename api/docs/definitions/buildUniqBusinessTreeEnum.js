const buildUniqueBusinessList = require('../../refs/buildUniqueBusinessList')

module.exports = (propertyName) => ({
  type: 'string',
  enum: buildUniqueBusinessList(propertyName)
})
