const countries = require('../../refs/countries')
module.exports = {
  type: 'string',
  enum: countries.map(country => country.countryCode).sort()
}
