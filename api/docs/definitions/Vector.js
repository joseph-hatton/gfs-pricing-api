const string = require('./string')
const ref = require('./ref')

module.exports = {
  properties: {
    legalEntity: ref('LegalEntities'),
    accountingBasis: string,
    durations: {
      type: 'array',
      items: ref('VectorDurations')
    }
  },
  required: [
    'accountingBasis',
    'legalEntity'
  ],
  additionalProperties: false
}
