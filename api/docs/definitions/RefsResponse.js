const string = require('./string')

const stringArray = {
  type: 'array',
  items: string
}

module.exports = {
  properties: {
    dealStatuses: stringArray,
    probCloses: stringArray,
    productTypes: stringArray,
    stages: stringArray
  },
  additionalProperties: false
}
