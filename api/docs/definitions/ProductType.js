const productTypes = require('../../refs/productTypes')

module.exports = {
  type: 'string',
  enum: productTypes.map(productType => productType.value)
}
