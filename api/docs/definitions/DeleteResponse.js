const uuid = require('./uuid')

module.exports = {
  properties: {
    uuid,
    deleted: {
      type: 'boolean'
    }
  }
}
