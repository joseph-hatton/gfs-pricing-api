const stages = require('../../refs/stages')

module.exports = {
  type: 'string',
  enum: stages
}
