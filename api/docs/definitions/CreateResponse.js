const string = require('./string')

module.exports = {
  properties: {
    id: string
  }
}
