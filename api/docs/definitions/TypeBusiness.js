const typeBusinesses = require('../../refs/typeBusinesses')

module.exports = {
  type: 'string',
  enum: typeBusinesses
}
