const nullableString = require('./nullableString')
const nullableDouble = require('./nullableDouble')
const integer = require('./integer')

module.exports = {
  properties: {
    Title: nullableString,
    fmnbev: nullableDouble,
    fmlev: nullableDouble,
    fmestrevenue: nullableDouble,
    otherfindetail: nullableString,
    CashCollCap: nullableString,
    fmtclosingdate: nullableString,
    fmtreffectivedate: nullableString,
    fmrgalegalentity: {
      type: ['array', 'null'],
      items: nullableString
    },
    legalEntities: {
      type: ['array', 'null'],
      items: nullableString
    },
    fmDescription: nullableString,
    fmStatus: nullableString,
    Last_x0020_Update: nullableString,
    By: nullableString,
    fmWaitingOn: nullableString,
    fmPriority: nullableString,
    fmStage: nullableString,
    fmProjectType: nullableString,
    fmRiskFinRe: nullableString,
    fmProduct: nullableString,
    fmMarket: nullableString,
    fmClientname: nullableString,
    fmGroupname: nullableString,
    fmDivision: nullableString,
    fmLikelihood: nullableString,
    fmUrgency: nullableString,
    fmValue: nullableString,
    Modified: nullableString,
    Created: nullableString,
    id: integer,
    url: nullableString
  },
  required: [
    'id'
  ],
  additionalProperties: true
}
