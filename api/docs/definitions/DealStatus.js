const dealStatuses = require('../../refs/dealStatuses')

module.exports = {
  type: 'string',
  enum: dealStatuses
}
