module.exports = {
  type: 'string',
  pattern: '^\\d{4}-([0]\\d|1[0-2])-([0-2]\\d|3[01])$'
}
