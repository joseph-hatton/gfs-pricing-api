const string = require('./string')
const nullableDouble = require('./nullableDouble')
const nullableString = require('./nullableString')
const ref = require('./ref')

module.exports = {
  properties: {
    legalEntity: ref('LegalEntities'),
    accountingBasis: string,
    acctBasis: string,
    businessUnit: nullableString,
    geoSegment: string,
    overrideMax: nullableDouble,
    overrideActual: nullableDouble,
    assetWAL: nullableDouble,
    initBenefitLiab: nullableDouble,
    initNetLiab: nullableDouble,
    initDTL: nullableDouble,
    initTotalCapital: nullableDouble,
    avgFactorC1: nullableDouble,
    avgFactorNonC1: nullableDouble,
    initAddlAssetLeverage: nullableDouble,
    avgAddlAssetLeverage: nullableDouble,
    acqExpAllowablePct: nullableDouble,
    acqExpAllowableAmt: nullableDouble,
    rocNY1: nullableDouble,
    rocNY2: nullableDouble,
    rocNY3: nullableDouble,
    rocNY4: nullableDouble,
    rocNY5: nullableDouble,
    rocNY6: nullableDouble,
    rocNY7: nullableDouble,
    rocNY8: nullableDouble,
    nbevNY1: nullableDouble,
    nbevNY2: nullableDouble,
    nbevNY3: nullableDouble,
    nbevNY4: nullableDouble,
    nbevNY5: nullableDouble,
    nbevNY6: nullableDouble,
    nbevNY7: nullableDouble,
    nbevNY8: nullableDouble,
    rocMean: nullableDouble,
    rocStdDev: nullableDouble,
    rocPercentile05: nullableDouble,
    rocPercentile10: nullableDouble,
    rocPercentile25: nullableDouble,
    rocPercentile50: nullableDouble,
    rocPercentile75: nullableDouble,
    rocPercentile90: nullableDouble,
    rocPercentile95: nullableDouble,
    nbevMean: nullableDouble,
    nbevStdDev: nullableDouble,
    nbevPercentile05: nullableDouble,
    nbevPercentile10: nullableDouble,
    nbevPercentile25: nullableDouble,
    nbevPercentile50: nullableDouble,
    nbevPercentile75: nullableDouble,
    nbevPercentile90: nullableDouble,
    nbevPercentile95: nullableDouble,
    descKeyRisk1: nullableString,
    stressKeyRisk1: nullableString,
    rocKeyRisk1: nullableDouble,
    nbevKeyRisk1: nullableDouble,
    descKeyRisk2: nullableString,
    stressKeyRisk2: nullableString,
    rocKeyRisk2: nullableDouble,
    nbevKeyRisk2: nullableDouble,
    descKeyRisk3: nullableString,
    stressKeyRisk3: nullableString,
    rocKeyRisk3: nullableDouble,
    nbevKeyRisk3: nullableDouble,
    descKeyRisk4: nullableString,
    stressKeyRisk4: nullableString,
    rocKeyRisk4: nullableDouble,
    nbevKeyRisk4: nullableDouble,
    descKeyRisk5: nullableString,
    stressKeyRisk5: nullableString,
    rocKeyRisk5: nullableDouble,
    nbevKeyRisk5: nullableDouble
  },
  required: [
    'accountingBasis'
  ],
  additionalProperties: false
}
