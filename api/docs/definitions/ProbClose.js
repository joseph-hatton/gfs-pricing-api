const probCloses = require('../../refs/probCloses')

module.exports = {
  type: 'string',
  enum: probCloses
}
