const string = require('./string')
const integer = require('./integer')
const ref = require('./ref')

module.exports = {
  properties: {
    name: string,
    code: ref('LegalEntity'),
    version: integer
  },
  required: [
    'code',
    'version'
  ],
  additionalProperties: false
}
