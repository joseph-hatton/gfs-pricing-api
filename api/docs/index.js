const definitions = require('./definitions')
const {version, description} = require('../../package.json')
const {port} = require('../config')

module.exports = {
  swagger: '2.0',
  info: {
    version,
    title: description
  },
  host: `localhost:${port}`,
  basePath: '/api/v1',
  schemes: [
    'https',
    'http'
  ],
  consumes: ['application/json'],
  produces: ['application/json'],
  definitions,
  paths: {}
}
