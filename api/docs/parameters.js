module.exports = {
  dealUuid: {
    name: 'dealUuid',
    in: 'path',
    type: 'string',
    required: true,
    description: 'the deal UUID'
  },
  userId: {
    name: 'userId',
    in: 'path',
    type: 'string',
    required: true,
    description: 'the user ID'
  }
}
