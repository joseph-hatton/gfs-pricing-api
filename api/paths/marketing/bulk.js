module.exports = () => ({
  PUT: require('../../marketing/bulk/updateMarketingEndpoint'),
  GET: require('../../marketing/bulk/getMarketingEndpoint')
})
