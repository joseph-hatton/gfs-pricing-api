module.exports = () => ({
  GET: require('../../deals/getDealEndpoint'),
  DELETE: require('../../deals/deleteDealEndpoint')
})
