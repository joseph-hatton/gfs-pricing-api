module.exports = () => ({
  GET: require('../deals/getDealsEndpoint'),
  POST: require('../deals/createDealEndpoint')
})
