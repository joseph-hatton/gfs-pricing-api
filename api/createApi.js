const openApi = require('express-openapi')
const rawApiDoc = require('./docs')

module.exports = (app, apiRoot) => {
  const {apiDoc} = openApi.initialize({
    app,
    apiDoc: rawApiDoc,
    paths: './api/paths'
  })
  app.get(`${apiRoot}/swagger.json`, ({headers: {host}}, res) => {
    res.json(Object.assign({}, apiDoc, {host, schemes: ['https']}))
  })
}
