module.exports = ({params: {userId}, headers}, res, next) => {
  const currentUserId = headers['user-id']
  if (currentUserId && currentUserId !== userId) {
    res.status(403).json({
      message: `user '${currentUserId}' is not authorized to read or update user data for '${userId}'`
    })
  } else {
    next()
  }
}
