const entitlements = {
  CREATE_DEAL: {},
  UPDATE_DEAL: {},
  DELETE_DEAL: {},
  UPDATE_MARKETING: {}
}

module.exports = entitlements
