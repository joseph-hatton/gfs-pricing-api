select
	deal.doc ->> 'dealName' as dealName,
	deal.version,
	vector.*

from vector
inner join deal
on
	vector.deal_uuid = deal.uuid
where vector.deal_uuid in (
	select
	  DISTINCT ON (doc -> 'dealName')
	  uuid
	from
	  sch_pricing.deal
	order by
	  doc->'dealName',
	  version desc
)
order by
	deal.doc -> 'dealName',
	vector.doc -> 'duration'