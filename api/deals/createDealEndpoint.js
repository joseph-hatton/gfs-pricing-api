const isString = require('lodash/isString')
const createDeal = require('./createDeal')
const getUserId = require('../getUserId')
const ref = require('../docs/definitions/ref')
const errors = require('../docs/errors')
const createEndpoint = require('../createEndpoint')

const create = async (doc, userId) => {
  if (!userId) {
    return {
      status: 401,
      message: 'there must be a current user to create a deal'
    }
  } else if (!isString(doc.dealName) || doc.dealName.trim().length === 0) {
    return {
      status: 400,
      message: `required property 'dealName' missing from deal document`
    }
  } else {
    return createDeal(doc, userId)
  }
}

const createDealEndpoint = async ({body, headers}, res) => {
  const {status, message, uuid} = await create(body, getUserId(headers))
  if (status) {
    res.status(status).json({message})
  } else {
    res.json({uuid})
  }
}

const apiDoc = {
  summary: 'Creates a new deal',
  operationId: 'createDeal',
  parameters: [
    {
      name: 'body',
      in: 'body',
      required: true,
      schema: ref('Deal')
    }
  ],
  responses: {
    200: {
      description: 'Deal was successfully created or updated',
      schema: ref('UuidResponse')
    },
    400: errors.create('JSON body was not a valid Deal'),
    401: errors.noCurrentUser,
    403: errors.dealNotAuthorized,
    500: errors.unexpected
  }
}

module.exports = createEndpoint(createDealEndpoint, apiDoc)
