require('require-sql')
const omit = require('lodash/omit')
const flatten = require('lodash/flatten')
const {pool} = require('../db')
const insertDealSql = require('./insert-deal.sql')
const insertVectorSql = require('./insert-vector.sql')
const hasEntitlement = require('../auth/hasEntitlement')
const {CREATE_DEAL} = require('../auth')
const log = require('../../log')
const getDealByDealName = require('./getDealByDealName')

const saveVectors = async (client, deal, dealUuid, userId) => {
  if (deal.vectors && deal.vectors.length > 0) {
    const vectorDurations = flatten(deal.vectors.map(vector => vector.durations.map(duration => ({company: vector.company, accountingBasis: vector.accountingBasis, ...duration}))))
    for (const vectorDuration of vectorDurations) {
      await client.query(insertVectorSql, [dealUuid, vectorDuration, userId])
    }
  }
}

const save = async (deal, userId) => {
  const existingDeal = await getDealByDealName(deal.dealName)
  const version = (existingDeal && existingDeal.version >= 1) ? existingDeal.version + 1 : 1

  const client = await pool.connect()
  try {
    await client.query('BEGIN')
    const dealWithScalars = omit(deal, 'vectors')
    log.info({dealName: deal.dealName, status: deal.status, effectiveDate: deal.effectiveDate, scalarCount: deal.accountingBasis.length, vectorCompanyCount: deal.vectors.length, userId}, 'inserting deal')
    const {uuid} = (await client.query(insertDealSql, [deal.id, dealWithScalars, version, userId])).rows[0]
    await saveVectors(client, deal, uuid, userId)
    await client.query('COMMIT')
    log.info({dealUuid: uuid}, 'successfully saved deal')
    return {uuid}
  } catch (err) {
    log.error({err}, 'error saving deal')
    await client.query('ROLLBACK')
    throw err
  } finally {
    client.release()
  }
}

module.exports = async (deal, userId) => {
  const canCreate = hasEntitlement(CREATE_DEAL, userId)
  if (canCreate) {
    if (!deal.accountingBasis) {
      deal.accountingBasis = []
    }
    if (!deal.vectors) {
      deal.vectors = []
    }
    const {uuid} = await save(deal, userId)
    return {uuid}
  } else {
    return {
      status: 403,
      message: `user '${userId}' is not authorized to create a deal`
    }
  }
}
