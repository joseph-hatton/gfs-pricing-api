module.exports = (dbRow) => ({
  ...dbRow.doc,
  uuid: dbRow.uuid,
  version: dbRow.version
})
