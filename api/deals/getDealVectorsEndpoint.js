const getDealVectorsByDeal = require('./getDealVectorsByDeal')
const errors = require('../docs/errors')
const parameters = require('../docs/parameters')
const ref = require('../docs/definitions/ref')
const createEndpoint = require('../createEndpoint')
const convertDbRowToResponse = require('./convertDbRowToResponse')

const getDealsEndpoint = async ({params: {dealUuid}}, res) => {
  const dbRows = await getDealVectorsByDeal(dealUuid)
  res.json(dbRows.map(convertDbRowToResponse))
}

const apiDoc = {
  summary: 'Responds with the vectors for a specific deal',
  operationId: 'getDealVectors',
  parameters: [
    parameters.dealUuid
  ],
  responses: {
    200: {
      description: 'an array of a deal\'s vectors',
      schema: {
        type: 'array',
        items: ref('Vector')
      }
    },
    500: errors.unexpected
  }
}

module.exports = createEndpoint(getDealsEndpoint, apiDoc)
