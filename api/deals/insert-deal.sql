insert into sch_pricing.deal (
  id,
  doc,
  version,
  last_modified_by
) values (
  $1,
  $2,
  $3,
  $4
)
returning uuid