require('require-sql')
const {query} = require('../db')
const sql = require('./delete-deal.sql')
const hasEntitlement = require('../auth/hasEntitlement')
const {DELETE_DEAL} = require('../auth')

module.exports = async (dealUuid, userId) => {
  const canDelete = hasEntitlement(DELETE_DEAL, userId)
  if (canDelete) {
    const {rowCount} = (await query(sql, [dealUuid]))
    return {
      uuid: dealUuid,
      deleted: (rowCount === 1)
    }
  } else {
    return {
      status: 403,
      message: `user '${userId}' is not authorized to delete deal '${dealUuid}'`
    }
  }
}
