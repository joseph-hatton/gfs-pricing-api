insert into sch_pricing.vector (
  deal_uuid,
  doc,
  last_modified_by
) values (
  $1,
  $2,
  $3
)
returning uuid