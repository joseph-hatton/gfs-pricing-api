select
  DISTINCT ON (doc -> 'dealName')
  *
from
  sch_pricing.deal
where
  doc ->> 'dealName' = $1
order by
  doc->'dealName',
  version desc