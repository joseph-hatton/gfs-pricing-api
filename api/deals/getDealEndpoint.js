const getDeal = require('./getDeal')
const errors = require('../docs/errors')
const parameters = require('../docs/parameters')
const ref = require('../docs/definitions/ref')
const createEndpoint = require('../createEndpoint')
const getUserId = require('../getUserId')
const convertDbRowToResponse = require('./convertDbRowToResponse')

const getDealEndpoint = async ({params: {dealUuid}, headers}, res) => {
  const rows = await getDeal(dealUuid, getUserId(headers))
  if (rows && rows.length === 1) {
    res.json(convertDbRowToResponse(rows[0]))
  } else {
    res.status(404).json({message: `no such deal: ${dealUuid}`})
  }
}

const apiDoc = {
  summary: 'Responds with a deal',
  operationId: 'getDeal',
  parameters: [
    parameters.dealUuid
  ],
  responses: {
    200: {
      description: 'a deal',
      schema: ref('Deal')
    },
    404: errors.dealNotFound,
    500: errors.unexpected
  }
}

module.exports = createEndpoint(getDealEndpoint, apiDoc)
