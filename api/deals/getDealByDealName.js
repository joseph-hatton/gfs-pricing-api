const {query} = require('../db')
require('require-sql')
const sql = require('./get-latest-deal-version-by-deal-name.sql')

module.exports = async (dealName, userId) => {
  const rows = (await query(sql, [dealName])).rows
  return (rows.length > 0) ? rows[0] : null
}
