  select
  DISTINCT ON (doc -> 'id', doc -> 'legalEntity' -> 'code' )
  *,
 exists (select 1 from sch_pricing.vector where deal_uuid = deal.uuid) as "hasVectors"
from
  sch_pricing.deal
order by
  doc->'id',
  doc -> 'legalEntity' -> 'code',
  version desc
