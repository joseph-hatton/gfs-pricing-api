const deleteDeal = require('./deleteDeal')
const getUserId = require('../getUserId')
const parameters = require('../docs/parameters')
const errors = require('../docs/errors')
const ref = require('../docs/definitions/ref')
const createEndpoint = require('../createEndpoint')

const deleteIt = async (dealUuid, userId) => {
  if (!userId) {
    return {
      status: 401,
      message: 'there must be a current user to delete a deal'
    }
  } else {
    return deleteDeal(dealUuid, userId)
  }
}

const deleteDealEndpoint = async ({params: {dealUuid}, headers}, res) => {
  const {uuid, deleted, status, message} = await deleteIt(dealUuid, getUserId(headers))
  if (status) {
    res.status(status).json({message})
  } else {
    res.json({uuid, deleted})
  }
}

const apiDoc = {
  summary: 'Deletes a deal',
  operationId: 'deleteDeal',
  parameters: [
    parameters.dealUuid
  ],
  responses: {
    200: {
      description: 'Deal was successfully or unsuccessfully deleted deleted',
      schema: ref('DeleteResponse')
    },
    401: errors.noCurrentUser,
    403: errors.dealNotAuthorized,
    500: errors.unexpected
  }
}

module.exports = createEndpoint(deleteDealEndpoint, apiDoc)
