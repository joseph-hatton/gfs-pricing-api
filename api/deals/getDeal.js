const {query} = require('../db')
require('require-sql')
const sql = require('./get-deal.sql')

module.exports = async (uuid, userId) => (await query(sql, [uuid])).rows
