const {query} = require('../db')
require('require-sql')
const sql = require('./get-latest-vector-versions.sql')

module.exports = async () => (await query(sql)).rows
