const getDeals = require('./getDeals')
const errors = require('../docs/errors')
const ref = require('../docs/definitions/ref')
const createEndpoint = require('../createEndpoint')

const convertDbRowToResponse = (dbRow) => ({
  ...dbRow.doc,
  uuid: dbRow.uuid,
  version: dbRow.version,
  hasVectors: dbRow.hasVectors || false
})

const getDealsEndpoint = async (req, res) => {
  const dbRows = await getDeals()
  res.json(dbRows.map(convertDbRowToResponse))
}

const apiDoc = {
  summary: 'Responds with all deals',
  operationId: 'getDeals',
  parameters: [],
  responses: {
    200: {
      description: 'an array of deals',
      schema: {
        type: 'array',
        items: ref('Deal')
      }
    },
    500: errors.unexpected
  }
}

module.exports = createEndpoint(getDealsEndpoint, apiDoc)
