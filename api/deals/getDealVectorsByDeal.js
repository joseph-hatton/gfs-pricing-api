const {query} = require('../db')
require('require-sql')
const sql = require('./get-vectors-by-deal.sql')

module.exports = async (dealUuid) => (await query(sql, [dealUuid])).rows
