module.exports = [
  { stage: 'Exploring', status: 'In Progress' },
  { stage: 'Negotiating', status: 'In Progress' },
  { stage: 'Analyzing', status: 'In Progress' },
  { stage: 'Active', status: 'In Progress' },
  { stage: 'Closing', status: 'In Progress' },
  { stage: 'Implementing', status: 'Final' },
  { stage: 'Completed', status: 'Final' },
  { stage: 'On Hold', status: 'Dead' },
  { stage: 'Lost', status: 'Dead' },
  { stage: 'Client Withdrawn', status: 'Dead' },
  { stage: 'RGA Withdrawn', status: 'Dead' }
]
