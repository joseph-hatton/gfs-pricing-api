const uniq = require('lodash/uniq')

const businessUnitTree = require('./businessUnitTree')

module.exports = (propertyName) => uniq(businessUnitTree.map(businessUnit => businessUnit[propertyName])).sort()
