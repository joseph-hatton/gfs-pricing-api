const productTypes = require('./productTypes')
const validProductTypes = productTypes.map(productType => productType.value)
const buildUniqueBusinessList = require('./buildUniqueBusinessList')
const offices = buildUniqueBusinessList('officeCode')
const currencies = buildUniqueBusinessList('currencyCode')
const legalEntities = buildUniqueBusinessList('legalEntityCode')
const businesses = buildUniqueBusinessList('businessName')
const geoSegments = buildUniqueBusinessList('geoSegmentCode')
const accountingBases = require('./accountingBases')
const typeBusinesses = require('./typeBusinesses')

const postTaxIncomeCalculationError = 'Post Tax Income input value does not equal calculated value'
const distributableEarningsCalculationError = 'Distributable Earnings input value does not equal calculated value'
const investedAssetsCalculationError = 'Invested Assets input value does not equal calculated value'

module.exports = {
  scalar: {
    template: 'Asset Intensive',
    tabName: 'UploadScalar',
    columnsToIgnore: ['revno', 'createddate', 'userid'],
    // legalEntityColumn: 'A',
    accountingBasisColumns: ['E', 'D', 'F'],
    accountingBasisColumn: 'B',
    fieldNameColumn: 'B',
    valueColumn: 'D',
    fields: {
      dealDetail: {
        description: { type: 'string' },
        productType: { type: 'string', required: true, valid: validProductTypes },
        businessUnit: { type: 'string', valid: businesses },
        currency: { type: 'string', required: true, valid: currencies },
        status: { type: 'string' },
        competitors: { type: 'integer' },
        typeBusiness: { type: 'string', valid: typeBusinesses },
        quotaShare: { type: 'number', required: true },
        nbevDeal: { type: 'number', required: true },
        levDeal: { type: 'number' },
        FYrev12mth: { type: 'number', required: true },
        FYptaoi12mth: { type: 'number', required: true },
        FYrevCalyr: { type: 'number', required: true },
        FYptaoiCalyr: { type: 'number', required: true },
        vectorInit: { type: 'number', required: true },
        vectorInitCount: { type: 'number', required: true },
        vectorUlt: { type: 'number', required: true },
        reinsForm: { type: 'string' },
        businessType: { type: 'string' },
        pricingDate: { type: 'date' },
        initGAAPNetLiab: { type: 'number' },
        peakGAAPNetLiab: { type: 'number' },
        initLocalRegLiab: { type: 'number' },
        annualPrem: { type: 'number' },
        initTier2: { type: 'number' },
        peakTier2: { type: 'number' },
        initCLCcollateral: { type: 'number' },
        peakCLCcollateral: { type: 'number' },
        initCapitalSP: { type: 'number' },
        peakCapitalSP: { type: 'number' },
        initCapitalEC: { type: 'number' },
        peakCapitalEC: { type: 'number' },
        irrCapitalSP: { type: 'number' },
        irrCapitalEC: { type: 'number' },
        initCapitalLocal: { type: 'number', requiredWithStatus: 'Final' },
        irrCapitalLocal: { type: 'number' },
        irrDDM: { type: 'number' },
        fundingBmk: { type: 'number' },
        collateralCost: { type: 'number' },
        rawCOF: { type: 'number' },
        optionalityAdjSP: { type: 'number' },
        cheapnessFundingSP: { type: 'number' },
        optionalityAdjEC: { type: 'number' },
        cheapnessFundingEC: { type: 'number' },
        roaAvgSP: { type: 'number' },
        roaAvgEC: { type: 'number' },
        premRate: { type: 'number' },
        initAssetsNetYield: { type: 'number' },
        initAssetsDuration: { type: 'number' },
        initAssetsAvgRating: { type: 'string' },
        initAssetsC1factorSP: { type: 'number' },
        initAssetsC1factorEC: { type: 'number' },
        avgAssetLevelYield: { type: 'number' },
        avgAssetLevelYieldexCG: { type: 'number' },
        liabModDuration: { type: 'number' },
        liabEffDuration: { type: 'number' },
        liabEffConvexity: { type: 'number' },
        initAssetsGrossYield: { type: 'number' },
        initAssetsDefaultChg: { type: 'number' },
        initAssetsMgmtFee: { type: 'number' },
        initAssetsConvexity: { type: 'number' },
        initAssetsOAS: { type: 'number' },
        peakLocalRegLiab: { type: 'number' },
        rawCOFspread: { type: 'number' }
      },
      companyAccountingBasis: {
        accountingBasis: { type: 'string', valid: accountingBases },
        businessUnit: { type: 'string', valid: businesses },
        office: { type: 'string', valid: offices },
        currency: { type: 'string', valid: currencies },
        legalEntity: { type: 'string', valid: legalEntities },
        geoSegment: { type: 'string', valid: geoSegments },
        nbevrate2: { type: 'number' },
        nbev2: { type: 'number' },
        nbevrate3: { type: 'number' },
        nbev3: { type: 'number' },
        lev: { type: 'number' },
        acctBasis: { type: 'string' },
        initLiabDuration: { type: 'number' },
        initBenefitLiab: { type: 'number' },
        initNetLiab: { type: 'number' },
        initDTL: { type: 'number' },
        initTotalCapital: { type: 'number' },
        avgFactorC1: { type: 'number' },
        avgFactorNonC1: { type: 'number' },
        initAddlAssetLeverage: { type: 'number' },
        avgAddlAssetLeverage: { type: 'number' },
        acqExpAllowablePct: { type: 'number' },
        acqExpAllowableAmt: { type: 'number' },
        rocNY1: { type: 'number' },
        rocNY2: { type: 'number' },
        rocNY3: { type: 'number' },
        rocNY4: { type: 'number' },
        rocNY5: { type: 'number' },
        rocNY6: { type: 'number' },
        rocNY7: { type: 'number' },
        rocNY8: { type: 'number' },
        nbevNY1: { type: 'number' },
        nbevNY2: { type: 'number' },
        nbevNY3: { type: 'number' },
        nbevNY4: { type: 'number' },
        nbevNY5: { type: 'number' },
        nbevNY6: { type: 'number' },
        nbevNY7: { type: 'number' },
        nbevNY8: { type: 'number' },
        rocMean: { type: 'number' },
        rocStdDev: { type: 'number' },
        rocPercentile05: { type: 'number' },
        rocPercentile10: { type: 'number' },
        rocPercentile25: { type: 'number' },
        rocPercentile50: { type: 'number' },
        rocPercentile75: { type: 'number' },
        rocPercentile90: { type: 'number' },
        rocPercentile95: { type: 'number' },
        nbevMean: { type: 'number' },
        nbevStdDev: { type: 'number' },
        nbevPercentile05: { type: 'number' },
        nbevPercentile10: { type: 'number' },
        nbevPercentile25: { type: 'number' },
        nbevPercentile50: { type: 'number' },
        nbevPercentile75: { type: 'number' },
        nbevPercentile90: { type: 'number' },
        nbevPercentile95: { type: 'number' },
        descKeyRisk1: { type: 'string' },
        stressKeyRisk1: { type: 'string' },
        rocKeyRisk1: { type: 'number' },
        nbevKeyRisk1: { type: 'number' },
        descKeyRisk2: { type: 'string' },
        stressKeyRisk2: { type: 'string' },
        rocKeyRisk2: { type: 'number' },
        nbevKeyRisk2: { type: 'number' },
        descKeyRisk3: { type: 'string' },
        stressKeyRisk3: { type: 'string' },
        rocKeyRisk3: { type: 'number' },
        nbevKeyRisk3: { type: 'number' },
        descKeyRisk4: { type: 'string' },
        stressKeyRisk4: { type: 'string' },
        rocKeyRisk4: { type: 'number' },
        nbevKeyRisk4: { type: 'number' },
        descKeyRisk5: { type: 'string' },
        stressKeyRisk5: { type: 'string' },
        rocKeyRisk5: { type: 'number' },
        nbevKeyRisk5: { type: 'number' },
        assetWAL: { type: 'number' },
        overrideMax: { type: 'number' },
        overrideActual: { type: 'number' }
      }
    }
  },
  vector: {
    tabName: 'UploadVector',
    durationColumn: 'A',
    startColumn: 'B',
    fields: {
      vector: {
        initPrem: { type: 'number', requiredWithStatus: 'Final' },
        excessSC: { type: 'number' },
        coi: { type: 'number' },
        policyCharge: { type: 'number' },
        invincFixed: { type: 'number' },
        hedgeIncome: { type: 'number' },
        otherRevenue: { type: 'number' },
        excessDB: { type: 'number' },
        otherBen: { type: 'number' },
        maintexpCedingco: { type: 'number' },
        commCedingco: { type: 'number' },
        commRGA: { type: 'number' },
        maintexpRGA: { type: 'number', requiredWithStatus: 'Final' },
        premtax: { type: 'number' },
        intCredFixed: { type: 'number' },
        intCredEquity: { type: 'number' },
        avbonus: { type: 'number' },
        cededReins: { type: 'number' },
        incrBenRes: { type: 'number' },
        incrResSOP: { type: 'number' },
        incrURR: { type: 'number' },
        decrDAC: { type: 'number' },
        decrSIA: { type: 'number' },
        tier2Charge: { type: 'number' },
        otherExpense: { type: 'number' },
        pretaxIncome: { type: 'number', requiredWithStatus: 'Final' },
        taxes: { type: 'number', requiredWithStatus: 'Final' },
        posttaxIncome: { type: 'number', requiredWithStatus: 'Final' },
        invincCap: { type: 'number', requiredWithStatus: 'Final' },
        taxInvincCap: { type: 'number', requiredWithStatus: 'Final' },
        incrCap: { type: 'number', requiredWithStatus: 'Final' },
        distEarn: { type: 'number', requiredWithStatus: 'Final' },
        assetsGA: { type: 'number', requiredWithStatus: 'Final' },
        assetsGAfixinc: { type: 'number', requiredWithStatus: 'Final' },
        DAC: { type: 'number' },
        assetsSA: { type: 'number' },
        SIA: { type: 'number' },
        fvHedge: { type: 'number' },
        assetsPolLoan: { type: 'number' },
        otherAsset: { type: 'number' },
        avTotal: { type: 'number' },
        avFixed: { type: 'number' },
        avIndexed: { type: 'number' },
        avSeparate: { type: 'number' },
        benRes: { type: 'number' },
        rsvSOP: { type: 'number' },
        hostContract: { type: 'number' },
        embedDeriv: { type: 'number' },
        URR: { type: 'number' },
        rsvNet: { type: 'number', requiredWithStatus: 'Final' },
        DTL: { type: 'number' },
        IMR: { type: 'number' },
        otherLiability: { type: 'number' },
        capC1pre: { type: 'number', requiredWithStatus: 'Final' },
        capC2: { type: 'number', requiredWithStatus: 'Final' },
        capC3: { type: 'number', requiredWithStatus: 'Final' },
        capC4: { type: 'number', requiredWithStatus: 'Final' },
        capital: { type: 'number', requiredWithStatus: 'Final' },
        ROA: { type: 'number' },
        capTier2: { type: 'number' },
        collPrcTrust: { type: 'number' },
        collCLC: { type: 'number' },
        premium: { type: 'number', requiredWithStatus: 'Final' },
        incIMR: { type: 'number' },
        feeIncome: { type: 'number' },
        deathben: { type: 'number' },
        fpw: { type: 'number' },
        surrAnn: { type: 'number' },
        riderben: { type: 'number' },
        incrRsvCoins: { type: 'number' },
        chgRsvModco: { type: 'number' },
        rsvGross: { type: 'number', requiredWithStatus: 'Final' },
        rsvCeded: { type: 'number' },
        rsvModco: { type: 'number' },
        rsvCoins: { type: 'number' },
        minTrustReq: { type: 'number' },
        avGA: { type: 'number' },
        rsvTax: { type: 'number' },
        rsvTaxCeded: { type: 'number' },
        csv: { type: 'number' }
      }
    },
    validation: {
      addSubtractCompare: {
        gaap: {
          postTaxIncome: {
            add: ['initPrem', 'excessSC', 'coi', 'policyCharge', 'invincFixed', 'hedgeIncome', 'otherRevenue'],
            subtract: ['excessDB', 'otherBen', 'maintexpCedingco', 'commCedingco', 'commRGA', 'maintexpRGA', 'premtax', 'intCredFixed', 'intCredEquity', 'avbonus', 'cededReins', 'incrBenRes', 'incrResSOP', 'incrURR', 'decrDAC', 'decrSIA', 'tier2Charge', 'otherExpense', 'taxes'],
            target: 'posttaxIncome',
            error: postTaxIncomeCalculationError
          },
          distributableEarnings: {
            add: ['posttaxIncome', 'invincCap'],
            subtract: ['taxInvincCap', 'incrCap'],
            target: 'distEarn',
            error: distributableEarningsCalculationError
          },
          investedAssets: {
            add: ['avTotal', 'benRes', 'rsvSOP', 'embedDeriv', 'URR', 'DTL', 'IMR', 'otherLiability', 'capital'],
            subtract: ['DAC', 'assetsSA', 'SIA', 'fvHedge', 'otherAsset'],
            target: 'assetsGA',
            error: investedAssetsCalculationError
          }
        },
        'localUs': {
          postTaxIncome: {
            add: ['premium', 'invincFixed', 'incIMR', 'hedgeIncome', 'feeIncome', 'otherRevenue'],
            subtract: ['deathben', 'fpw', 'surrAnn', 'riderben', 'commRGA', 'maintexpCedingco', 'commCedingco', 'maintexpRGA', 'premtax', 'cededReins', 'incrRsvCoins', 'chgRsvModco', 'otherExpense', 'taxes'],
            target: 'posttaxIncome',
            error: postTaxIncomeCalculationError
          },
          distributableEarnings: {
            add: ['posttaxIncome', 'invincCap'],
            subtract: ['taxInvincCap', 'incrCap'],
            target: 'distEarn',
            error: distributableEarningsCalculationError
          },
          investedAssets: {
            add: ['rsvGross', 'IMR', 'otherLiability', 'capital'],
            subtract: ['rsvCeded', 'assetsSA', 'fvHedge', 'otherAsset'],
            target: 'assetsGA',
            error: investedAssetsCalculationError
          }
        }
      }
    }
  }
}
