module.exports = [{
  countryName: 'Angola',
  countryCode: 'AGO'

}, {
  countryName: 'Burundi',
  countryCode: 'BDI'

}, {
  countryName: 'Benin',
  countryCode: 'BEN'

}, {
  countryName: 'BurkinaFaso',
  countryCode: 'BFA'

}, {
  countryName: 'Botswana',
  countryCode: 'BWA'

}, {
  countryName: 'CentralAfricanRepublic',
  countryCode: 'CAF'

}, {
  countryName: 'Cote D\'Ivoire ',
  countryCode: 'CIV'

}, {
  countryName: 'Cameroon',
  countryCode: 'CMR'

}, {
  countryName: 'CongoDemocraticRep.ofthe',
  countryCode: 'COD'

}, {
  countryName: 'Congo',
  countryCode: 'COG'

}, {
  countryName: 'Comoros',
  countryCode: 'COM'

}, {
  countryName: 'CapeVerde',
  countryCode: 'CPV'

}, {
  countryName: 'Djibouti',
  countryCode: 'DJI'

}, {
  countryName: 'Algeria',
  countryCode: 'DZA'

}, {
  countryName: 'Egypt',
  countryCode: 'EGY'

}, {
  countryName: 'Eritrea',
  countryCode: 'ERI'

}, {
  countryName: 'Western Sahara',
  countryCode: 'ESH'
}, {
  countryName: 'Ethiopia',
  countryCode: 'ETH'
}, {
  countryName: 'Gabon',
  countryCode: 'GAB'

}, {
  countryName: 'Ghana',
  countryCode: 'GHA'

}, {
  countryName: 'Guinea',
  countryCode: 'GIN'

}, {
  countryName: 'Gambia',
  countryCode: 'GMB'

}, {
  countryName: 'Guinea-Bissau',
  countryCode: 'GNB'

}, {
  countryName: 'EquatorialGuinea',
  countryCode: 'GNQ'

}, {
  countryName: 'Kenya',
  countryCode: 'KEN'

}, {
  countryName: 'Liberia',
  countryCode: 'LBR'

}, {
  countryName: 'TheStateofLibya',
  countryCode: 'LBY'

}, {
  countryName: 'Lesotho',
  countryCode: 'LSO'

}, {
  countryName: 'Morocco',
  countryCode: 'MAR'

}, {
  countryName: 'Madagascar',
  countryCode: 'MDG'

}, {
  countryName: 'Mali',
  countryCode: 'MLI'

}, {
  countryName: 'Mozambique',
  countryCode: 'MOZ'

}, {
  countryName: 'Mauritania',
  countryCode: 'MRT'

}, {
  countryName: 'Mauritius',
  countryCode: 'MUS'

}, {
  countryName: 'Malawi',
  countryCode: 'MWI'

}, {
  countryName: 'Mayotte',
  countryCode: 'MYT'

}, {
  countryName: 'Namibia',
  countryCode: 'NAM'

}, {
  countryName: 'Niger',
  countryCode: 'NER'

}, {
  countryName: 'Nigeria',
  countryCode: 'NGA'

}, {
  countryName: 'Reunion',
  countryCode: 'REU'

}, {
  countryName: 'Rwanda',
  countryCode: 'RWA'

}, {
  countryName: 'Sudan',
  countryCode: 'SDN'

}, {
  countryName: 'Senegal',
  countryCode: 'SEN'

}, {
  countryName: 'SaintHelena',
  countryCode: 'SHN'

}, {
  countryName: 'SierraLeone',
  countryCode: 'SLE'

}, {
  countryName: 'Somalia',
  countryCode: 'SOM'

}, {
  countryName: 'SaoTomeandPrincipe',
  countryCode: 'STP'

}, {
  countryName: 'Swaziland',
  countryCode: 'SWZ'

}, {
  countryName: 'Seychelles',
  countryCode: 'SYC'

}, {
  countryName: 'Chad',
  countryCode: 'TCD'

}, {
  countryName: 'Togo',
  countryCode: 'TGO'

}, {
  countryName: 'Tunisia',
  countryCode: 'TUN'

}, {
  countryName: 'TanzaniaUnitedRepublicof',
  countryCode: 'TZA'

}, {
  countryName: 'Uganda',
  countryCode: 'UGA'

}, {
  countryName: 'SouthAfrica',
  countryCode: 'ZAF'

}, {
  countryName: 'Zambia',
  countryCode: 'ZMB'

}, {
  countryName: 'Zimbabwe',
  countryCode: 'ZWE'

}, {
  countryName: 'Antarctica',
  countryCode: 'ATA'

}, {
  countryName: 'S.Georgia&S.SandwichIsl',
  countryCode: 'SGS'

}, {
  countryName: 'Afghanistan',
  countryCode: 'AFG'

}, {
  countryName: 'UnitedArabEmirates',
  countryCode: 'ARE'

}, {
  countryName: 'Armenia',
  countryCode: 'ARM'

}, {
  countryName: 'Azerbaijan',
  countryCode: 'AZE'

}, {
  countryName: 'Bangladesh',
  countryCode: 'BGD'

}, {
  countryName: 'Bahrain',
  countryCode: 'BHR'

}, {
  countryName: 'BruneiDarussalam',
  countryCode: 'BRN'

}, {
  countryName: 'Bhutan',
  countryCode: 'BTN'

}, {
  countryName: 'Cocos(Keeling)Islands',
  countryCode: 'CCK'

}, {
  countryName: 'China',
  countryCode: 'CHN'

}, {
  countryName: 'ChristmasIsland',
  countryCode: 'CXR'

}, {
  countryName: 'Georgia',
  countryCode: 'GEO'

}, {
  countryName: 'HongKong',
  countryCode: 'HKG'

}, {
  countryName: 'Indonesia',
  countryCode: 'IDN'

}, {
  countryName: 'India',
  countryCode: 'IND'

}, {
  countryName: 'BritishIndianOceanTerritory',
  countryCode: 'IOT'

}, {
  countryName: 'IranIslamicRepublicOf',
  countryCode: 'IRN'

}, {
  countryName: 'Iraq',
  countryCode: 'IRQ'

}, {
  countryName: 'Israel',
  countryCode: 'ISR'

}, {
  countryName: 'Jordan',
  countryCode: 'JOR'

}, {
  countryName: 'Japan',
  countryCode: 'JPN'

}, {
  countryName: 'Kazakhstan',
  countryCode: 'KAZ'

}, {
  countryName: 'Kyrgyzstan',
  countryCode: 'KGZ'

}, {
  countryName: 'Cambodia',
  countryCode: 'KHM'

}, {
  countryName: 'KoreaRepublicof',
  countryCode: 'KOR'

}, {
  countryName: 'Kuwait',
  countryCode: 'KWT'

}, {
  countryName: 'LaoPpl\'sDemocraticRepublic ',
  countryCode: 'LAO'

}, {
  countryName: 'Lebanon',
  countryCode: 'LBN'

}, {
  countryName: 'SriLanka',
  countryCode: 'LKA'

}, {
  countryName: 'Macao',
  countryCode: 'MAC'

}, {
  countryName: 'Maldives',
  countryCode: 'MDV'

}, {
  countryName: 'Myanmar',
  countryCode: 'MMR'

}, {
  countryName: 'Mongolia',
  countryCode: 'MNG'

}, {
  countryName: 'Malaysia',
  countryCode: 'MYS'

}, {
  countryName: 'Nepal',
  countryCode: 'NPL'

}, {
  countryName: 'Oman',
  countryCode: 'OMN'

}, {
  countryName: 'Pakistan',
  countryCode: 'PAK'

}, {
  countryName: 'Philippines',
  countryCode: 'PHL'

}, {
  countryName: 'KoreaDemocraticPpl\'s Repub ',
  countryCode: 'PRK'

}, {
  countryName: 'PalestinianTerritoryOccupied',
  countryCode: 'PSE'

}, {
  countryName: 'Qatar',
  countryCode: 'QAT'

}, {
  countryName: 'SaudiArabia',
  countryCode: 'SAU'

}, {
  countryName: 'Singapore',
  countryCode: 'SGP'

}, {
  countryName: 'SyrianArabRepublic',
  countryCode: 'SYR'

}, {
  countryName: 'Thailand',
  countryCode: 'THA'

}, {
  countryName: 'Tajikistan',
  countryCode: 'TJK'

}, {
  countryName: 'Turkmenistan',
  countryCode: 'TKM'

}, {
  countryName: 'Turkey',
  countryCode: 'TUR'

}, {
  countryName: 'TaiwanRepublicofChina',
  countryCode: 'TWN'

}, {
  countryName: 'Uzbekistan',
  countryCode: 'UZB'

}, {
  countryName: 'VietNam',
  countryCode: 'VNM'

}, {
  countryName: 'Yemen',
  countryCode: 'YEM'

}, {
  countryName: 'AmericanSamoa',
  countryCode: 'ASM'

}, {
  countryName: 'Australia',
  countryCode: 'AUS'

}, {
  countryName: 'CookIslands',
  countryCode: 'COK'

}, {
  countryName: 'Fiji',
  countryCode: 'FJI'

}, {
  countryName: 'MicronesiaFederatedStates',
  countryCode: 'FSM'

}, {
  countryName: 'Guam',
  countryCode: 'GUM'

}, {
  countryName: 'HeardandMcDonaldIslands',
  countryCode: 'HMD'

}, {
  countryName: 'Kiribati',
  countryCode: 'KIR'

}, {
  countryName: 'MarshallIslands',
  countryCode: 'MHL'

}, {
  countryName: 'NorthernMarianaIslands',
  countryCode: 'MNP'

}, {
  countryName: 'NewCaledonia',
  countryCode: 'NCL'

}, {
  countryName: 'NorfolkIsland',
  countryCode: 'NFK'

}, {
  countryName: 'Niue',
  countryCode: 'NIU'

}, {
  countryName: 'Nauru',
  countryCode: 'NRU'

}, {
  countryName: 'NewZealand',
  countryCode: 'NZL'

}, {
  countryName: 'Pitcairn',
  countryCode: 'PCN'

}, {
  countryName: 'Palau',
  countryCode: 'PLW'

}, {
  countryName: 'PapuaNewGuinea',
  countryCode: 'PNG'

}, {
  countryName: 'FrenchPolynesia',
  countryCode: 'PYF'

}, {
  countryName: 'SolomonIslands',
  countryCode: 'SLB'

}, {
  countryName: 'Tokelau',
  countryCode: 'TKL'

}, {
  countryName: 'Timor-Leste',
  countryCode: 'TLS'

}, {
  countryName: 'Tonga',
  countryCode: 'TON'

}, {
  countryName: 'Tuvalu',
  countryCode: 'TUV'

}, {
  countryName: 'USMinorOutlyingIslands',
  countryCode: 'UMI'

}, {
  countryName: 'Vanuatu',
  countryCode: 'VUT'

}, {
  countryName: 'WallisandFutunaIslands',
  countryCode: 'WLF'

}, {
  countryName: 'Samoa',
  countryCode: 'WSM'

}, {
  countryName: 'AlandIslands',
  countryCode: 'ALA'

}, {
  countryName: 'Albania',
  countryCode: 'ALB'

}, {
  countryName: 'Andorra',
  countryCode: 'AND'

}, {
  countryName: 'FrenchSouthernTerritories',
  countryCode: 'ATF'

}, {
  countryName: 'Austria',
  countryCode: 'AUT'

}, {
  countryName: 'Belgium',
  countryCode: 'BEL'

}, {
  countryName: 'Bulgaria',
  countryCode: 'BGR'

}, {
  countryName: 'BosniaandHerzegovina',
  countryCode: 'BIH'

}, {
  countryName: 'Belarus',
  countryCode: 'BLR'

}, {
  countryName: 'BouvetIsland',
  countryCode: 'BVT'

}, {
  countryName: 'Switzerland',
  countryCode: 'CHE'

}, {
  countryName: 'Cyprus',
  countryCode: 'CYP'

}, {
  countryName: 'CzechRepublic',
  countryCode: 'CZE'

}, {
  countryName: 'Germany',
  countryCode: 'DEU'

}, {
  countryName: 'Denmark',
  countryCode: 'DNK'

}, {
  countryName: 'Spain',
  countryCode: 'ESP'

}, {
  countryName: 'Estonia',
  countryCode: 'EST'

}, {
  countryName: 'EuropeanUnion',
  countryCode: 'EUN'

}, {
  countryName: 'Finland',
  countryCode: 'FIN'

}, {
  countryName: 'France',
  countryCode: 'FRA'

}, {
  countryName: 'FaroeIslands',
  countryCode: 'FRO'

}, {
  countryName: 'UnitedKingdom',
  countryCode: 'GBR'

}, {
  countryName: 'Guernsey',
  countryCode: 'GGY'

}, {
  countryName: 'Gibraltar',
  countryCode: 'GIB'

}, {
  countryName: 'Greece',
  countryCode: 'GRC'

}, {
  countryName: 'Croatia',
  countryCode: 'HRV'

}, {
  countryName: 'Hungary',
  countryCode: 'HUN'

}, {
  countryName: 'IsleofMan',
  countryCode: 'IMN'

}, {
  countryName: 'Ireland',
  countryCode: 'IRL'

}, {
  countryName: 'Iceland',
  countryCode: 'ISL'

}, {
  countryName: 'Italy',
  countryCode: 'ITA'

}, {
  countryName: 'Jersey',
  countryCode: 'JEY'

}, {
  countryName: 'Liechtenstein',
  countryCode: 'LIE'

}, {
  countryName: 'Lithuania',
  countryCode: 'LTU'

}, {
  countryName: 'Luxembourg',
  countryCode: 'LUX'

}, {
  countryName: 'Latvia',
  countryCode: 'LVA'

}, {
  countryName: 'Monaco',
  countryCode: 'MCO'

}, {
  countryName: 'MoldovaRepublicof',
  countryCode: 'MDA'

}, {
  countryName: 'MacedoniaTheFormerYugoslavRep.of',
  countryCode: 'MKD'

}, {
  countryName: 'Malta',
  countryCode: 'MLT'

}, {
  countryName: 'Montenegro',
  countryCode: 'MNE'

}, {
  countryName: 'Netherlands',
  countryCode: 'NLD'

}, {
  countryName: 'Norway',
  countryCode: 'NOR'

}, {
  countryName: 'Poland',
  countryCode: 'POL'

}, {
  countryName: 'Portugal',
  countryCode: 'PRT'

}, {
  countryName: 'Romania',
  countryCode: 'ROU'

}, {
  countryName: 'RussianFederation',
  countryCode: 'RUS'

}, {
  countryName: 'SvalbardandJanMayen',
  countryCode: 'SJM'

}, {
  countryName: 'SanMarino',
  countryCode: 'SMR'

}, {
  countryName: 'Serbia',
  countryCode: 'SRB'

}, {
  countryName: 'Slovakia',
  countryCode: 'SVK'

}, {
  countryName: 'Slovenia',
  countryCode: 'SVN'

}, {
  countryName: 'Sweden',
  countryCode: 'SWE'

}, {
  countryName: 'Ukraine',
  countryCode: 'UKR'

}, {
  countryName: 'HolySee(VaticanCityState)',
  countryCode: 'VAT'

}, {
  countryName: 'Multinational',
  countryCode: 'MNL'

}, {
  countryName: 'Supranational',
  countryCode: 'SPR'

}, {
  countryName: 'Aruba',
  countryCode: 'ABW'

}, {
  countryName: 'Anguilla',
  countryCode: 'AIA'

}, {
  countryName: 'NetherlandsAntilles',
  countryCode: 'ANT'

}, {
  countryName: 'AntiguaandBarbuda',
  countryCode: 'ATG'

}, {
  countryName: 'BonaireSaintEustatiusandSaba',
  countryCode: 'BES'

}, {
  countryName: 'Bahamas',
  countryCode: 'BHS'

}, {
  countryName: 'SaintBarthelemy',
  countryCode: 'BLM'

}, {
  countryName: 'Belize',
  countryCode: 'BLZ'

}, {
  countryName: 'Bermuda',
  countryCode: 'BMU'

}, {
  countryName: 'Barbados',
  countryCode: 'BRB'

}, {
  countryName: 'Canada',
  countryCode: 'CAN'

}, {
  countryName: 'CostaRica',
  countryCode: 'CRI'

}, {
  countryName: 'Cuba',
  countryCode: 'CUB'

}, {
  countryName: 'Curacao',
  countryCode: 'CUW'

}, {
  countryName: 'CaymanIslands',
  countryCode: 'CYM'

}, {
  countryName: 'Dominica',
  countryCode: 'DMA'

}, {
  countryName: 'DominicanRepublic',
  countryCode: 'DOM'

}, {
  countryName: 'Guadeloupe',
  countryCode: 'GLP'

}, {
  countryName: 'Grenada',
  countryCode: 'GRD'

}, {
  countryName: 'Greenland',
  countryCode: 'GRL'

}, {
  countryName: 'Guatemala',
  countryCode: 'GTM'

}, {
  countryName: 'Honduras',
  countryCode: 'HND'

}, {
  countryName: 'Haiti',
  countryCode: 'HTI'

}, {
  countryName: 'Jamaica',
  countryCode: 'JAM'

}, {
  countryName: 'SaintKittsandNevis',
  countryCode: 'KNA'

}, {
  countryName: 'SaintLucia',
  countryCode: 'LCA'

}, {
  countryName: 'SaintMartin',
  countryCode: 'MAF'

}, {
  countryName: 'Mexico',
  countryCode: 'MEX'

}, {
  countryName: 'Montserrat',
  countryCode: 'MSR'

}, {
  countryName: 'Martinique',
  countryCode: 'MTQ'

}, {
  countryName: 'Nicaragua',
  countryCode: 'NIC'

}, {
  countryName: 'Panama',
  countryCode: 'PAN'

}, {
  countryName: 'PuertoRico',
  countryCode: 'PRI'

}, {
  countryName: 'ElSalvador',
  countryCode: 'SLV'

}, {
  countryName: 'SaintPierreandMiquelon',
  countryCode: 'SPM'

}, {
  countryName: 'SintMaarten(Dutchpart)',
  countryCode: 'SXM'

}, {
  countryName: 'TurksandCaicosIslands',
  countryCode: 'TCA'

}, {
  countryName: 'TrinidadandTobago',
  countryCode: 'TTO'

}, {
  countryName: 'UnitedStates',
  countryCode: 'USA'

}, {
  countryName: 'StVincentandtheGrenadines',
  countryCode: 'VCT'

}, {
  countryName: 'VirginIslandsBritish',
  countryCode: 'VGB'

}, {
  countryName: 'VirginIslandsU.S.',
  countryCode: 'VIR'

}, {
  countryName: 'Argentina',
  countryCode: 'ARG'

}, {
  countryName: 'Bolivia',
  countryCode: 'BOL'

}, {
  countryName: 'Brazil',
  countryCode: 'BRA'

}, {
  countryName: 'Chile',
  countryCode: 'CHL'

}, {
  countryName: 'Colombia',
  countryCode: 'COL'

}, {
  countryName: 'Ecuador',
  countryCode: 'ECU'

}, {
  countryName: 'FalklandIslands(Malvinas)',
  countryCode: 'FLK'

}, {
  countryName: 'FrenchGuiana',
  countryCode: 'GUF'

}, {
  countryName: 'Guyana',
  countryCode: 'GUY'

}, {
  countryName: 'Peru',
  countryCode: 'PER'

}, {
  countryName: 'Paraguay',
  countryCode: 'PRY'

}, {
  countryName: 'Suriname',
  countryCode: 'SUR'

}, {
  countryName: 'Uruguay',
  countryCode: 'URY'

}, {
  countryName: 'Venezuela',
  countryCode: 'VEN'
}]
