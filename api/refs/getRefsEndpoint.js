const errors = require('../docs/errors')
const ref = require('../docs/definitions/ref')
const createEndpoint = require('../createEndpoint')
const accountingBases = require('./accountingBases')
const dealStatuses = require('./dealStatuses')
const previewValuesNames = require('./previewValuesNames')
const urlRef = require('./urlRef')
const probCloses = require('./probCloses')
const productTypes = require('./productTypes')
const countries = require('./countries')
const stages = require('./stages')
const typeBusinesses = require('./typeBusinesses')
const businessUnitTree = require('./businessUnitTree')
const uploadConfig = require('./uploadConfig')
const uploadConfigLongevity = require('./uploadConfigLongevity')

const getRefsEndpoint = (req, res) => {
  res.json({
    accountingBases,
    dealStatuses,
    previewValuesNames,
    probCloses,
    productTypes,
    countries,
    stages,
    typeBusinesses,
    businessUnitTree,
    uploadConfig: uploadConfig,
    uploadConfigLongevity: uploadConfigLongevity,
    urlRef
  })
}

const apiDoc = {
  summary: 'Responds with all reference data',
  operationId: 'getRefs',
  parameters: [],
  responses: {
    200: {
      description: 'reference data',
      schema: {
        type: 'object',
        items: ref('RefsResponse')
      }
    },
    500: errors.unexpected
  }
}

module.exports = createEndpoint(getRefsEndpoint, apiDoc)
