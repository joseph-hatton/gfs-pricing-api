module.exports = [
  { name: 'productType', description: 'Product Type', order: 1, format: null },
  { name: 'currency', description: 'Currency', order: 2, format: null },
  { name: 'nbevDeal', description: 'NBEV', order: 3, format: 'numberFormat' },
  { name: 'initCapitalEC', description: 'Initial Economic Capital', order: 4, format: 'numberFormat' },
  { name: 'initCapitalSP', description: 'Initial S&P Capital', order: 5, format: 'numberFormat' },
  { name: 'initCapitalLocal', description: 'Initial Local Reg Capital', order: 6, format: 'numberFormat' },
  { name: 'FYrev12mth', description: 'FY Revenue (12 months)', order: 7, format: 'numberFormat' },
  { name: 'FYrevCalyr', description: 'FY Revenue (Curr Cal Yr)', order: 8, format: 'numberFormat' },
  { name: 'FYptaoi12mth', description: 'FY PTAOI (12 months)', order: 9, format: 'numberFormat' },
  { name: 'FYptaoiCalyr', description: 'FY PTAOI (Curr Cal Yr)', order: 10, format: 'numberFormat' }
]
