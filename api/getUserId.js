const {env} = process

let localUserEnabled = env.NODE_ENV !== 'production'

const localUser = env.USERNAME || env.USER || 'unknown'

const getUserId = headers =>
  headers['user-id'] || (localUserEnabled ? localUser : undefined)

// noinspection JSUnusedGlobalSymbols
module.exports = Object.assign(
  getUserId,
  {
    setLocalUserEnabled: enabled => {
      localUserEnabled = enabled
    },
    localUser
  }
)
