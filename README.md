#Introduction 
This is the back-end of Pricing API.  Spreadsheets will be parsed and sent over to the API to the MongoDB database. 

* Database
  * [PostgreSQL 10+](https://www.postgresql.org/)
    * Keeping schemas in sync with [Flyway](https://flywaydb.org/)
      * data/flyway/*
* Hosts restful API's at [deployed host]/api/v1/deals
* Hosts a Tableau Web Data Connector at [deployed host]/wdc

---

# Run locally

**Pre-Requisites:** Install Docker, nodejs 8.4+

There is a docker-compose file to help run the postgres locally and to ensure the flyway is run.
* `npm run postgres:start`
  * **Note:** If you already have the postgres started and want to run the flyway again, just run the same start command.  

Now you can run the api locally

```
  npm install

  npm run dev
```

* Local URL: http://localhost:3006/api/v1/deals
* Swagger: http://localhost:3006/api/swagger.json
* WebDataConnector: http://localhost:3006/wdc
* Dev WebDataConnector: https://gfs-pricing-api-dev.rgare.net/wdc/

---

## Test the web data connector locally

* Instructions taken from http://tableau.github.io/webdataconnector/docs/

```
git clone https://github.com/tableau/webdataconnector.git
cd webdataconnector
npm install --production
npm start
```
Open a browser to: http://localhost:8888/Simulator/index.html

Then paste that in the `Connector URL` http://localhost:3006/wdc into its input box

---

#Build and Test
TODO: Describe and show how to build your code and run the tests. 

---

#Contribute
Business: GFS Pricing Team.
Developers: Joseph Hatton 
