FROM stlpartifact01.rgare.net:7443/rga-node-alpine-89-lts
RUN apk update && \
    apk add bash curl vim
WORKDIR /app
COPY . /app
RUN npm install --production
EXPOSE 3006
#ENTRYPOINT /bin/sh
CMD ["npm", "start"]
