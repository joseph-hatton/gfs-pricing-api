const bunyan = require('bunyan')

const log = bunyan.createLogger({name: 'gfs-pricing-api'})

module.exports = log
