const express = require('express')
const compression = require('compression')
const bodyParser = require('body-parser')
const log = require('./log')
const multer = require('multer')
const {port, uploadFilePath} = require('./api/config')
const errorMiddleware = require('./errorMiddleware')
const exit = require('./exit')
const createApi = require('./api/createApi')
const addMiddleware = require('./api/addMiddleware')

const app = express()
app.use(compression())

if (process.env.NODE_ENV !== 'production') {
  const cors = require('cors')
  app.use(cors())
}

const apiRoot = '/api'

app.use(apiRoot, bodyParser.json({limit: '5mb'}))
app.use(multer({dest: uploadFilePath}).fields([{name: 'file'}]))
app.get('/health', require('./api/healthcheck'))
addMiddleware(app, `${apiRoot}/v1`)

try {
  createApi(app, apiRoot)
  app.use(errorMiddleware)
  app.listen(port, () => log.info(`running: http://localhost:${port}`))
} catch (e) {
  log.error(e)
  exit(1)
}

// const errorHandler = (error, req, res, next) => {
//   if (error) {
//     error.stack && log.error(error.stack)
//     res.status(500).json(error)
//   } else {
//     next()
//   }
// }

// swaggerExpress.create({appRoot: __dirname}, (error, swagger) => {
//   if (error) {
//     log.error('failed to initialize swagger', util.inspect(error, {depth: null, color: true}))
//   } else {
//     app.get(`${apiPath}/swagger.json`, require(`./api/swagger/jsonEndpoint`))
//     app.use('/wdc', wdcRoutes)
//     app.use(apiPath, bodyParser.json({
//       limit: '5mb'
//     }))
//     app.use(multer({dest: uploadFilePath}).fields([{name: 'file'}]))
//     swagger.register(app)
//     app.use(errorHandler)
//     app.listen(port, () => log.info(`running: http://localhost:${port}`))
//   }
// })
