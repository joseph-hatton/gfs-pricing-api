-- Create marketing table
CREATE TABLE sch_pricing.marketing
(
  id                INTEGER                  NOT NULL,
  doc               JSONB                    NOT NULL,
  last_modified     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  last_modified_by  TEXT                     NOT NULL,
  created           TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  PRIMARY KEY (id)
);

ALTER TABLE sch_pricing.marketing
  OWNER TO sch_pricing;

CREATE TRIGGER marketing_sync_last_modified
BEFORE UPDATE
  ON sch_pricing.marketing
FOR EACH ROW
EXECUTE PROCEDURE sch_pricing.sync_last_modified();

GRANT SELECT, UPDATE, INSERT, DELETE ON sch_pricing.marketing TO app_pricing;
