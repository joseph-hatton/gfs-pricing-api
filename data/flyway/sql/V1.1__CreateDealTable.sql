-- Create the last modified trigger function
CREATE FUNCTION sch_pricing.sync_last_modified()
  RETURNS TRIGGER
LANGUAGE 'plpgsql'
COST 100.0
VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
    NEW.last_modified := NOW();
    RETURN NEW;
END;
$BODY$;

ALTER FUNCTION sch_pricing.sync_last_modified()
OWNER TO sch_pricing;
GRANT EXECUTE ON FUNCTION sch_pricing.sync_last_modified() TO PUBLIC;
GRANT EXECUTE ON FUNCTION sch_pricing.sync_last_modified() TO sch_pricing WITH GRANT OPTION;

-- Create deal table
CREATE TABLE sch_pricing.deal
(
  uuid              UUID                     NOT NULL DEFAULT uuid_generate_v4(),
  doc               JSONB                    NOT NULL,
  last_modified     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  last_modified_by  TEXT                     NOT NULL,
  created           TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  PRIMARY KEY (uuid)
);

ALTER TABLE sch_pricing.deal
  OWNER TO sch_pricing;

CREATE INDEX deal_uuid
  ON sch_pricing.deal USING BTREE
  (uuid ASC NULLS LAST);

CREATE INDEX deal_name
  ON sch_pricing.deal((doc ->> 'dealName'));

CREATE TRIGGER deal_sync_last_modified
BEFORE UPDATE
  ON sch_pricing.deal
FOR EACH ROW
EXECUTE PROCEDURE sch_pricing.sync_last_modified();

GRANT SELECT, UPDATE, INSERT, DELETE ON sch_pricing.deal TO app_pricing;
