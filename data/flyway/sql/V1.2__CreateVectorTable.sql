-- Create vector table
CREATE TABLE sch_pricing.vector
(
  uuid              UUID                     NOT NULL DEFAULT uuid_generate_v4(),
  deal_uuid         UUID                     NOT NULL,
  doc               JSONB                    NOT NULL,
  last_modified     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  last_modified_by  TEXT                     NOT NULL,
  created           TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  PRIMARY KEY (uuid)
);

ALTER TABLE sch_pricing.vector
  OWNER TO sch_pricing;

CREATE INDEX vector_uuid
  ON sch_pricing.vector USING BTREE
  (uuid ASC NULLS LAST);

CREATE INDEX company_business_duration
  ON sch_pricing.vector((doc ->> 'company'), (doc ->> 'accountingBasis'), (doc ->> 'duration'));

ALTER TABLE sch_pricing.vector
  ADD CONSTRAINT fk_deal
FOREIGN KEY (deal_uuid)
REFERENCES sch_pricing.deal(uuid);

CREATE TRIGGER deal_sync_last_modified
BEFORE UPDATE
  ON sch_pricing.vector
FOR EACH ROW
EXECUTE PROCEDURE sch_pricing.sync_last_modified();

GRANT SELECT, UPDATE, INSERT, DELETE ON sch_pricing.vector TO app_pricing;
