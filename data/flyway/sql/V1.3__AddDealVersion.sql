-- Deal Versioning
ALTER TABLE sch_pricing.deal
  ADD COLUMN version
  SMALLINT
  NOT NULL
  DEFAULT 0;

CREATE UNIQUE INDEX deal_unique_dealname_version on sch_pricing.deal( (doc->'dealName'), version)