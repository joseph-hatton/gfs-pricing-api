CREATE INDEX vector_deal_uuid
  ON sch_pricing.vector USING BTREE
  (deal_uuid ASC NULLS LAST);