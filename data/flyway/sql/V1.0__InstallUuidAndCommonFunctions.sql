CREATE USER app_pricing WITH PASSWORD 'abc123';
-- create schema 
CREATE SCHEMA IF NOT EXISTS sch_pricing AUTHORIZATION sch_pricing;
GRANT ALL PRIVILEGES ON SCHEMA sch_pricing TO app_pricing;
GRANT ALL ON schema sch_pricing TO sch_pricing;
-- Install the UUID extension
CREATE EXTENSION "uuid-ossp" SCHEMA sch_pricing;