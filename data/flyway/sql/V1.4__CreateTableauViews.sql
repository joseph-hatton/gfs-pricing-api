CREATE view sch_pricing.v_deals
AS
SELECT
	d.uuid,
	d.version,
	d.last_modified_by,
	d.created,
	dealDoc.*
FROM
	deal d
CROSS JOIN LATERAL
	jsonb_to_record(d.doc)
AS dealDoc(
	"dealName" varchar,
	"description" text,
	"productType" varchar,
	"businessUnit" varchar,
	"office" varchar,
	"currency" varchar,
	"legalEntity" varchar,
	"geoSegment" varchar,
	"country" varchar,
	"status" varchar,
	"stage" varchar,
	"effectiveDate" date,
	"expCloseDate" date,
	"probClose" varchar,
	"competitors" integer,
	"typeBusiness" varchar,
	"quotaShare" decimal,
	"nbevDeal" decimal,
	"levDeal" decimal,
	"FYrev12mth" decimal,
	"FYptaoi12mth" decimal,
	"FYrevCalyr" decimal,
	"FYptaoiCalyr" decimal,
	"vectorInit" integer,
	"vectorInitCount" integer,
	"vectorUlt" integer
);
GRANT SELECT ON sch_pricing.v_deals TO app_pricing;

CREATE VIEW sch_pricing.v_company_accounting_bases
AS
SELECT
	d.uuid,
	d.doc ->> 'dealName' dealName,
	d.version,
	d.last_modified_by,
	d.created,
	s.*
FROM
	deal d
CROSS JOIN LATERAL
	jsonb_to_recordset(d.doc -> 'companyAccountingBases')
AS s(
	"company" varchar,
	"accountingBasis" varchar,
	"businessUnit" varchar,
	"office" varchar,
	"currency" varchar,
	"initCapital" decimal,
	"irrCapital" decimal,
	"nbevrate1" decimal,
	"nbev1" decimal,
	"nbevrate2" decimal,
	"nbev2" decimal,
	"nbevrate3" decimal,
	"nbev3" decimal,
	"lev" decimal,
	"fundingBmk" decimal,
	"collateralAdj" decimal,
	"rawCOF" decimal,
	"optionalityAdj" decimal,
	"cheapnessFunding" decimal,
	"overrideMax" decimal,
	"overrideActual" decimal,
	"premMargin" decimal,
	"initGAAPNetLiab" decimal,
	"cirAV" decimal,
	"assetWAL" decimal,
	"assetAddlLeverage" decimal,
	"assetAddlLeverageInit" decimal,
	"roa0" decimal,
	"roaAvg" decimal,
	"roe" decimal,
	"irrAssets" decimal,
	"irrLiabCFpretax" decimal,
	"irrLiabCF" decimal,
	"leverageRatio" decimal,
	"leverageAvg" decimal,
	"initStatResv" decimal,
	"initIMR" decimal,
	"initCedingComm" decimal,
	"initConsideration" decimal,
	"initCollateral" decimal,
	"initLiabDuration" decimal
)
ORDER BY
	d.doc ->> 'dealName',
	d.version asc;
GRANT SELECT ON sch_pricing.v_company_accounting_bases TO app_pricing;

CREATE view sch_pricing.v_vectors
AS
SELECT
	d.uuid "dealUuid",
	d.doc ->> 'dealName' as dealName,
	d.version,
	d.last_modified_by,
	d.created,
	v.uuid "vectorUuid",
	vectorDoc.*
FROM
	vector v,
	deal d,
	jsonb_to_record(v.doc)
AS vectorDoc(
	"company" varchar,
	"accountingBasis" varchar,
	"duration" integer,
	"initPrem" decimal,
	"excessSC" decimal,
	"coi" decimal,
	"policyCharge" decimal,
	"invincFixed" decimal,
	"hedgeIncome" decimal,
	"otherRevenue" decimal,
	"excessDB" decimal,
	"otherBen" decimal,
	"maintexpCedingco" decimal,
	"commCedingco" decimal,
	"commRGA" decimal,
	"maintexpRGA" decimal,
	"premtax" decimal,
	"intCredFixed" decimal,
	"intCredEquity" decimal,
	"avbonus" decimal,
	"cededReins" decimal,
	"incrBenRes" decimal,
	"incrResSOP" decimal,
	"incrURR" decimal,
	"decrDAC" decimal,
	"decrSIA" decimal,
	"tier2Charge" decimal,
	"otherExpense" decimal,
	"pretaxIncome" decimal,
	"taxes" decimal,
	"posttaxIncome" decimal,
	"invincCap" decimal,
	"taxInvincCap" decimal,
	"incrCap" decimal,
	"distEarn" decimal,
	"assetsGA" decimal,
	"DAC" decimal,
	"assetsSA" decimal,
	"SIA" decimal,
	"fvHedge" decimal,
	"assetsPolLoan" decimal,
	"otherAsset" decimal,
	"avTotal" decimal,
	"avFixed" decimal,
	"avIndexed" decimal,
	"avSeparate" decimal,
	"benRes" decimal,
	"rsvSOP" decimal,
	"hostContract" decimal,
	"embedDeriv" decimal,
	"URR" decimal,
	"rsvNet" decimal,
	"DTL" decimal,
	"IMR" decimal,
	"otherLiability" decimal,
	"capC1pre" decimal,
	"capC2" decimal,
	"capC3" decimal,
	"capC4" decimal,
	"capital" decimal,
	"ROA" decimal,
	"capTier2" decimal,
	"collPrcTrust" decimal,
	"collCLC" decimal,
	"premium" decimal,
	"incIMR" decimal,
	"feeIncome" decimal,
	"deathben" decimal,
	"fpw" decimal,
	"surrAnn" decimal,
	"riderben" decimal,
	"incrRsvCoins" decimal,
	"chgRsvModco" decimal,
	"assetsGAfixinc" decimal,
	"rsvGross" decimal,
	"rsvCeded" decimal,
	"rsvModco" decimal,
	"rsvCoins" decimal,
	"minTrustReq" decimal,
	"avGA" decimal,
	"rsvTax" decimal,
	"rsvTaxCeded" decimal,
	"csv" decimal
)
WHERE
	v.deal_uuid = d.uuid
ORDER BY
	d.doc ->> 'dealName',
	d.version,
	vectorDoc.company,
	vectorDoc."accountingBasis",
	vectorDoc.duration ASC;

GRANT SELECT ON sch_pricing.v_vectors TO app_pricing;