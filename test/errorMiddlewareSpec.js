const proxyquire = require('./proxyquire')
const mockResponse = require('./mockResponse')

describe('errorMiddleware', () => {
  let log, errorMiddleware, error, res, next

  beforeEach(() => {
    log = {error: sinon.stub()}
    errorMiddleware = proxyquire('errorMiddleware', {
      './log': log
    })
    error = new Error('!')
    res = mockResponse()
    next = sinon.stub()
  })

  const apply = () => errorMiddleware(error, {}, res, next)

  it('invokes next if error is not defined', () => {
    error = undefined
    apply()

    next.should.have.been.calledWithExactly()
    res.status
  })

  it('sends 500 when unexpected error occurs', () => {
    apply()

    next.should.not.have.been.called
    res.status.should.have.been.calledWithExactly(500)
    res.json.should.have.been.calledWithExactly(error)
    log.error.should.have.been.calledWithExactly(error)
  })

  it('sends 400 when openapi validation fails', () => {
    error.status = 400
    error.errors = [{}]
    apply()

    res.status.should.have.been.calledWithExactly(400)
    res.json.should.have.been.calledWithExactly({
      message: 'Open API validation failed',
      errors: error.errors
    })
    log.error.should.not.have.been.called
  })
})
