const createEndpoint = require('../../api/createEndpoint')

describe('createEndpoint', () => {
  let fn, apiDoc, endpoint, next

  beforeEach(() => {
    fn = sinon.stub()
    apiDoc = {fake: true}
    endpoint = createEndpoint(fn, apiDoc)
    next = sinon.stub()
  })

  const apply = () => endpoint('req', 'res', next)

  it('invokes handler function', async () => {
    await apply()

    fn.should.have.been.calledWithExactly('req', 'res')
    next.should.not.have.been.called
  })

  it('invokes next when handler function throws an error', async () => {
    const error = new Error('!')
    fn.withArgs('req', 'res').rejects(error)
    await apply()

    next.should.have.been.calledWithExactly(error)
  })
})
