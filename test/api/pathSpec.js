const fs = require('fs')
const path = require('path')
const toPairs = require('lodash/toPairs')
const fromPairs = require('lodash/fromPairs')
const range = require('lodash/range')
const sum = require('lodash/sum')
const size = require('lodash/size')
const proxyquire = require('../proxyquire')

const appRoot = path.resolve(__dirname, '../..')

const endpointNameRegex = /^.*Endpoint.js$/

const ignoreSet = new Set(['createEndpoint.js', 'versionJsonEndpoint.js'])

const isEndpoint = fileName => !ignoreSet.has(fileName) && endpointNameRegex.exec(fileName)

const expected = {
  'deals': {
    get: 'deals/getDeals',
    post: 'deals/createDeal'
  },
  'deals/{dealUuid}': {
    get: 'deals/getDeal',
    delete: 'deals/deleteDeal'
  },
  'deals/{dealUuid}/vectors': {
    get: 'deals/getDealVectors'
  },
  'refs': {
    get: 'refs/getRefs'
  }
}

describe('paths', () => {
  Object.keys(expected).forEach(rawPath => {
    describe(rawPath, () => {
      const path = rawPath.split('/')

      const resolve = handlerPath => range(0, path.length).map(() => '..').join('/') + `/${handlerPath}Endpoint`

      it('matches expected endpoints', () => {
        const resolvedEndpoints = fromPairs(
          toPairs(expected[rawPath]).map(([method, handlerPath]) =>
            [
              method,
              resolve(handlerPath)
            ]
          )
        )
        const dependencies = fromPairs(Object.values(resolvedEndpoints).map(path => [path, sinon.stub()]))
        const expectedHandlers = fromPairs(
          toPairs(resolvedEndpoints).map(([method, resolvedPath]) => [
            method.toUpperCase(),
            dependencies[resolvedPath]
          ])
        )
        const handlers = proxyquire(['api', 'paths', ...path].join('/'), dependencies)()

        handlers.should.eql(expectedHandlers)
      })
    })
  })

  const getEndpointCount = dirPath => {
    console.log(`dir path ${dirPath}`)
    const contents = fs.readdirSync(dirPath).map(name => ({
      name,
      dir: fs.statSync(path.resolve(dirPath, name)).isDirectory()
    }))
    const endpoints = contents.filter(({name, dir}) => !dir && isEndpoint(name))
    return endpoints.length + sum(
      contents.filter(_ => _.dir).map(_ =>
        getEndpointCount(path.resolve(dirPath, _.name))
      )
    )
  }

  xit('maps all endpoints in code', () => {
    const expectedEndpointCount = sum(Object.keys(expected).map(_ => size(expected[_])))

    getEndpointCount(path.resolve(appRoot, 'api')).should.equal(expectedEndpointCount)
  })
})
