const addMiddleware = require('../../api/addMiddleware')

describe('addMiddleware', () => {
  let app
  beforeEach(() => {
    app = {use: sinon.stub()}
  })

  it('should add no middleware currently', () => {
    addMiddleware(app, 'apiroot')
    app.use.should.not.have.been.called
  })
})
