const rawApiDoc = require('../../api/docs')
const proxyquire = require('../proxyquire')
const mockResponse = require('../mockResponse')

describe('createApi', () => {
  let openApi, app, createApi, apiDoc

  const apiRoot = '/api'

  beforeEach(() => {
    openApi = {initialize: sinon.stub()}
    createApi = proxyquire('api/createApi', {
      'express-openapi': openApi
    })
    app = {get: sinon.stub()}
    apiDoc = {fake: true}
    openApi.initialize.returns({apiDoc})
  })

  const apply = () => createApi(app, apiRoot)

  it('initializes api', () => {
    apply()

    openApi.initialize.should.have.been.calledWithExactly({
      app,
      apiDoc: rawApiDoc,
      paths: './api/paths'
    })
  })

  it('adds swagger.json endpoint', () => {
    const req = {headers: {host: 'http://myhost'}}
    const res = mockResponse()
    apply()

    app.get.should.have.been.calledWith(`${apiRoot}/swagger.json`)
    app.get.lastCall.args[1](req, res)
    res.json.should.have.been.calledWithExactly(
      Object.assign({}, apiDoc, {host: req.headers.host, schemes: ['https']})
    )
  })
})
