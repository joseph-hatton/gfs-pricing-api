const proxyquire = require('../../proxyquire')
const mockResponse = require('../../mockResponse')

describe('getDealEndpoint', () => {
  let getDealEndpoint, getDeal, getUserId, convertDbRowToResponse, req, res
  beforeEach(() => {
    getDeal = sinon.stub().resolves([{uuid: 'abc', doc: {dealName: 'a big deal'}}])
    getUserId = sinon.stub().returns('userA')
    res = mockResponse()
    convertDbRowToResponse = sinon.stub().returns({uuid: 'abc', dealName: 'a big deal'})
    getDealEndpoint = proxyquire('api/deals/getDealEndpoint', {
      './getDeal': getDeal,
      '../getUserId': getUserId,
      './convertDbRowToResponse': convertDbRowToResponse
    })
    req = {
      params: {
        dealUuid: 'abc'
      },
      headers: 'some headers',
      body: {
      }
    }
  })

  const apply = () => getDealEndpoint(req, res)

  it('get deal called', async () => {
    await apply()

    getDeal.should.have.been.calledWithExactly('abc', 'userA')
  })

  it('sends response', async () => {
    await apply()

    res.json.should.have.been.calledWithExactly({dealName: 'a big deal', uuid: 'abc'})
  })

  it('sends no such deal if not found', async () => {
    getDeal.resolves([])

    await apply()

    res.status.should.have.been.calledWithExactly(404)
    res.json.should.have.been.calledWithExactly({message: 'no such deal: abc'})
  })
})
