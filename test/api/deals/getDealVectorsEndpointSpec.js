const proxyquire = require('../../proxyquire')
const mockResponse = require('../../mockResponse')

describe('getDealVectorsEndpoint', () => {
  let getDealVectorsEndpoint, getDealVectorsByDeal, convertDbRowToResponse, req, res
  beforeEach(() => {
    getDealVectorsByDeal = sinon.stub().resolves([{uuid: 'abc', doc: {duration: 12}}])
    res = mockResponse()
    convertDbRowToResponse = sinon.stub().returns({uuid: 'abc', duration: 12})
    getDealVectorsEndpoint = proxyquire('api/deals/getDealVectorsEndpoint', {
      './getDealVectorsByDeal': getDealVectorsByDeal,
      './convertDbRowToResponse': convertDbRowToResponse
    })
    req = {
      params: {
        dealUuid: 'abc'
      },
      headers: 'some headers',
      body: {
      }
    }
  })

  const apply = () => getDealVectorsEndpoint(req, res)

  it('get deal vectors called', async () => {
    await apply()

    getDealVectorsByDeal.should.have.been.calledWithExactly('abc')
  })

  it('sends response', async () => {
    await apply()

    res.json.should.have.been.calledWithExactly([{duration: 12, uuid: 'abc'}])
  })

  it('sends no such deal if not found', async () => {
    getDealVectorsByDeal.resolves([])

    await apply()

    res.json.should.have.been.calledWithExactly([])
  })
})
