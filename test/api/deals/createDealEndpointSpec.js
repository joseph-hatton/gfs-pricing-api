const proxyquire = require('../../proxyquire')
const mockResponse = require('../../mockResponse')

describe('createDealEndpoint', () => {
  let createDealEndpoint, createDeal, getUserId, req, res
  beforeEach(() => {
    createDeal = sinon.stub().returns(true)
    getUserId = sinon.stub().returns('userA')
    res = mockResponse()
    createDealEndpoint = proxyquire('api/deals/createDealEndpoint', {
      './createDeal': createDeal,
      '../getUserId': getUserId
    })
    req = {
      params: {},
      headers: 'some headers',
      body: {
        dealName: 'the name'
      }
    }
  })

  const apply = () => createDealEndpoint(req, res)

  it('create deal called', async () => {
    await apply()

    createDeal.should.have.been.calledWithExactly(req.body, 'userA')
  })

  it('requires a userid', async () => {
    getUserId.returns(null)

    await apply()

    res.status.should.have.been.calledWithExactly(401)
    res.json.should.have.been.calledWithExactly({message: 'there must be a current user to create a deal'})
  })

  it('requires a dealName', async () => {
    req.body.dealName = ' '

    await apply()

    res.status.should.have.been.calledWithExactly(400)
    res.json.should.have.been.calledWithExactly({message: 'required property \'dealName\' missing from deal document'})
  })
})
