require('require-sql')
const cloneDeep = require('lodash/cloneDeep')
const omit = require('lodash/omit')
const proxyquire = require('../../proxyquire')
const {CREATE_DEAL} = require('../../../api/auth')
const insertDealSql = require('../../../api/deals/insert-deal.sql')
const insertVectorSql = require('../../../api/deals/insert-vector.sql')
const mockDb = require('../db/mockDb')

describe('createDeal', () => {
  let createDeal, query, release, hasEntitlement, getDealByDealName, deal, vectors, expectedDurationOnSave
  beforeEach(() => {
    hasEntitlement = sinon.stub().returns(true)
    getDealByDealName = sinon.stub().resolves(null)
    deal = {dealName: 'yep', id: 123}
    vectors = [{
      company: 'companyA',
      accountingBasis: 'accBasisA',
      durations: [{duration: 0, initPrem: 5}]
    }]
    expectedDurationOnSave = {
      company: 'companyA',
      accountingBasis: 'accBasisA',
      duration: 0,
      initPrem: 5
    }
    const mockedDb = mockDb()
    query = mockedDb.query
    query.resolves({rows: [{uuid: 'abc'}]})
    release = mockedDb.release
    createDeal = proxyquire('api/deals/createDeal', {
      '../db': mockedDb,
      '../auth/hasEntitlement': hasEntitlement,
      './getDealByDealName': getDealByDealName
    })
  })

  const apply = () => createDeal(cloneDeep(deal), 'userA')

  it('has entitlement called', async () => {
    await apply()

    hasEntitlement.should.have.been.calledWithExactly(CREATE_DEAL, 'userA')
  })

  it('release called', async () => {
    await apply()

    release.should.have.been.called
  })

  it('query called with deal and vector sql', async () => {
    deal.vectors = vectors

    await apply()

    query.should.have.been.called
    query.args[0][0].should.eql('BEGIN')
    query.args[1][0].should.eql(insertDealSql)
    query.args[1][1].should.eql([123, omit({...deal, accountingBasis: []}, 'vectors'), 1, 'userA'])
    query.args[2][0].should.eql(insertVectorSql)
    query.args[2][1].should.eql(['abc', expectedDurationOnSave, 'userA'])
    query.args[3][0].should.eql('COMMIT')
  })

  it('query called with next version if one found', async () => {
    getDealByDealName.resolves({version: 1})
    deal.vectors = vectors

    await apply()

    query.should.have.been.called
    query.args[1][1].should.eql([123, omit({...deal, accountingBasis: []}, 'vectors'), 2, 'userA'])
  })

  it('rollback transaction', async () => {
    deal.vectors = vectors
    query.onCall(1).rejects(new Error('!'))
    try {
      await apply()
    } catch (err) {
      query.should.have.been.called
      query.args[0][0].should.eql('BEGIN')
      query.args[1][0].should.eql(insertDealSql)
      query.args[1][1].should.eql([123, omit({'id': 123, dealName: 'yep', accountingBasis: []}, 'vectors'), 1, 'userA'])
      query.args[2][0].should.eql('ROLLBACK')
    }
  })

  it('uuid eqls', async () => {
    const {uuid} = await apply()

    uuid.should.eql('abc')
  })

  it('returns error without proper entitlement', async () => {
    hasEntitlement.returns(false)

    const {status, message} = await apply()

    status.should.eql(403)
    message.should.eql('user \'userA\' is not authorized to create a deal')
  })
})
