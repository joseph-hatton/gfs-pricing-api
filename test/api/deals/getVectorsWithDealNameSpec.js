require('require-sql')
const proxyquire = require('../../proxyquire')
const sql = require('../../../api/deals/get-latest-vector-versions.sql')
const mockDb = require('../db/mockDb')

describe('getVectorsWithDealName', () => {
  let getVectorsWithDealName, query, vector
  beforeEach(() => {
    vector = {uuid: 'abc', dealName: 'yep'}
    const mockedDb = mockDb()
    query = mockedDb.query
    query.resolves({rows: [vector]})
    getVectorsWithDealName = proxyquire('api/deals/getVectorsWithDealName', {
      '../db': mockedDb
    })
  })

  const apply = () => getVectorsWithDealName()

  it('query called with sql', async () => {
    await apply()

    query.should.have.been.calledWithExactly(sql)
  })

  it('get vectors with deal name', async () => {
    const results = await apply()

    results.should.eql([vector])
  })
})
