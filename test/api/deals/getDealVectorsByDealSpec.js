require('require-sql')
const proxyquire = require('../../proxyquire')
const sql = require('../../../api/deals/get-vectors-by-deal.sql')
const mockDb = require('../db/mockDb')

const uuid = 'abc-123'
describe('getDealVectorsByDeal', () => {
  let getDealVectorsByDeal, query, vectors
  beforeEach(() => {
    vectors = [

    ]
    const mockedDb = mockDb()
    query = mockedDb.query
    query.resolves({rows: vectors})
    getDealVectorsByDeal = proxyquire('api/deals/getDealVectorsByDeal', {
      '../db': mockedDb
    })
  })

  const apply = () => getDealVectorsByDeal(uuid)

  it('query called with sql', async () => {
    await apply()

    query.should.have.been.calledWithExactly(sql, [uuid])
  })

  it('get deal vectors by deal uuid', async () => {
    const vectors = await apply()

    vectors.should.eql(vectors)
  })

  it('no deal vectors found', async () => {
    query.resolves({rows: []})
    const vectors = await apply()

    vectors.should.eql([])
  })
})
