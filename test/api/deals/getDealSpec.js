require('require-sql')
const proxyquire = require('../../proxyquire')
const sql = require('../../../api/deals/get-deal.sql')
const mockDb = require('../db/mockDb')

describe('getDeal', () => {
  let getDeals, query, deal, uuid
  beforeEach(() => {
    deal = {dealName: 'yep'}
    uuid = 'abc123'
    const mockedDb = mockDb()
    query = mockedDb.query
    query.resolves({rows: [deal]})
    getDeals = proxyquire('api/deals/getDeal', {
      '../db': mockedDb
    })
  })

  const apply = () => getDeals(uuid)

  it('query called with sql', async () => {
    await apply()

    query.should.have.been.calledWithExactly(sql, [uuid])
  })

  it('get deal', async () => {
    const deal = await apply()

    deal.should.eql(deal)
  })
})
