require('require-sql')
const proxyquire = require('../../proxyquire')
const sql = require('../../../api/deals/get-latest-deal-version-by-deal-name.sql')
const mockDb = require('../db/mockDb')

describe('getDealByDealName', () => {
  let getDeals, query, deal
  beforeEach(() => {
    deal = {dealName: 'yep'}
    const mockedDb = mockDb()
    query = mockedDb.query
    query.resolves({rows: [deal]})
    getDeals = proxyquire('api/deals/getDealByDealName', {
      '../db': mockedDb
    })
  })

  const apply = () => getDeals(deal.dealName, 'userA')

  it('query called with sql', async () => {
    await apply()

    query.should.have.been.calledWithExactly(sql, ['yep'])
  })

  it('get deal by deal name', async () => {
    const deal = await apply()

    deal.should.eql(deal)
  })

  it('no deal found', async () => {
    query.resolves({rows: []})
    const deal = await apply()

    should.not.exist(deal)
  })
})
