require('require-sql')
const proxyquire = require('../../proxyquire')
const {DELETE_DEAL} = require('../../../api/auth')
const sql = require('../../../api/deals/delete-deal.sql')

describe('deleteDeal', () => {
  const dealUuid = 'abc'
  let deleteDeal, query, hasEntitlement
  beforeEach(() => {
    query = sinon.stub().resolves({rowCount: 1})
    hasEntitlement = sinon.stub().returns(true)

    deleteDeal = proxyquire('api/deals/deleteDeal', {
      '../db': {
        query: query
      },
      '../auth/hasEntitlement': hasEntitlement
    })
  })

  const apply = () => deleteDeal(dealUuid, 'userA')

  it('has entitlement called', async () => {
    await apply()

    hasEntitlement.should.have.been.calledWithExactly(DELETE_DEAL, 'userA')
  })

  it('has entitlement called', async () => {
    await apply()

    query.should.have.been.calledWithExactly(sql, [dealUuid])
  })

  it('returns uuid and successful delete', async () => {
    const {uuid, deleted} = await apply()

    uuid.should.eql('abc')
    deleted.should.eql(true)
  })

  it('returns uuid and unsuccessful delete', async () => {
    query.resolves({rowCount: 0})

    const {uuid, deleted} = await apply()

    uuid.should.eql('abc')
    deleted.should.eql(false)
  })

  it('returns error without proper entitlement', async () => {
    hasEntitlement.returns(false)

    const {status, message} = await apply()

    status.should.eql(403)
    message.should.eql('user \'userA\' is not authorized to delete deal \'abc\'')
  })
})
