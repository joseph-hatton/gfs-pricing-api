require('require-sql')
const proxyquire = require('../../proxyquire')
const sql = require('../../../api/deals/get-latest-deal-versions.sql')
const mockDb = require('../db/mockDb')

describe('getDeal', () => {
  let getDeals, query, deal
  beforeEach(() => {
    deal = {uuid: 'abc', dealName: 'yep'}
    const mockedDb = mockDb()
    query = mockedDb.query
    query.resolves({rows: [deal]})
    getDeals = proxyquire('api/deals/getDeals', {
      '../db': mockedDb
    })
  })

  const apply = () => getDeals()

  it('query called with sql', async () => {
    await apply()

    query.should.have.been.calledWithExactly(sql)
  })

  it('get deal', async () => {
    const deals = await apply()

    deals.should.eql([deal])
  })
})
