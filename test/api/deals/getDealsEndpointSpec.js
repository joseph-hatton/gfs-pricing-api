const proxyquire = require('../../proxyquire')
const mockResponse = require('../../mockResponse')

describe('getDealsEndpoint', () => {
  let getDealEndpoint, getDeals, getUserId, req, res
  beforeEach(() => {
    getDeals = sinon.stub().resolves([{uuid: 'abc', version: 1, doc: {dealName: 'a big deal'}}])
    getUserId = sinon.stub().returns('userA')
    res = mockResponse()
    getDealEndpoint = proxyquire('api/deals/getDealsEndpoint', {
      './getDeals': getDeals,
      '../getUserId': getUserId
    })
    req = {
      params: {
      },
      headers: 'some headers',
      body: {
        dealName: 'the name'
      }
    }
  })

  const apply = () => getDealEndpoint(req, res)

  it('get deals called', async () => {
    await apply()

    getDeals.should.have.been.calledWithExactly()
  })

  it('sends response', async () => {
    await apply()

    res.json.should.have.been.calledWithExactly([{dealName: 'a big deal', uuid: 'abc', version: 1, hasVectors: false}])
  })

  it('sends empty array if no results', async () => {
    getDeals.resolves([])

    await apply()

    res.json.should.have.been.calledWithExactly([])
  })
})
