const proxyquire = require('../../proxyquire')
const mockResponse = require('../../mockResponse')

describe('deleteDealEndpoint', () => {
  let deleteDealEndpoint, deleteDeal, getUserId, req, res

  beforeEach(() => {
    deleteDeal = sinon.stub().returns({uuid: 'abc', deleted: true})
    getUserId = sinon.stub().returns('userA')
    res = mockResponse()
    deleteDealEndpoint = proxyquire('api/deals/deleteDealEndpoint', {
      './deleteDeal': deleteDeal,
      '../getUserId': getUserId
    })
    req = {
      params: {
        dealUuid: 'abc'
      },
      headers: 'some headers',
      body: {
        dealName: 'the name'
      }
    }
  })

  const apply = () => deleteDealEndpoint(req, res)

  it('delete deal called', async () => {
    await apply()

    deleteDeal.should.have.been.calledWithExactly('abc', 'userA')
  })

  it('requires a userid', async () => {
    getUserId.returns(null)

    await apply()

    res.status.should.have.been.calledWithExactly(401)
    res.json.should.have.been.calledWithExactly({message: 'there must be a current user to delete a deal'})
  })
})
