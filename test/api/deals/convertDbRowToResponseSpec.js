const convertDbRowToResponse = require('../../../api/deals/convertDbRowToResponse')

describe('convertDbRowToResponse', () => {
  it('gets doc and uuid', () => {
    const result = convertDbRowToResponse({uuid: 'abc', version: 2, doc: {one: 1, two: 2}, ignore: 'yes'})

    result.should.eql({
      uuid: 'abc',
      one: 1,
      two: 2,
      version: 2
    })
  })
})
