const proxyquire = require('../../proxyquire')
const mockResponse = require('../../mockResponse')
const mockLogs = require('../../mockLogs')

describe('healthcheck', () => {
  let healthcheck, res, getDeals, log
  beforeEach(() => {
    res = mockResponse()
    log = mockLogs()
    getDeals = sinon.stub().resolves({hello: true})
    healthcheck = proxyquire('api/healthcheck', {
      '../../log': log,
      '../deals/getDeals': getDeals
    })
  })

  const apply = () => healthcheck({}, res)

  it('should return ok', async () => {
    await apply()

    res.json.should.have.been.calledWithExactly({ok: true})
  })

  it('should return ok', async () => {
    const error = new Error('!')
    getDeals.rejects(error)

    await apply()

    log.error.should.have.been.calledWithExactly(error)
    res.status.should.have.been.calledWithExactly(500)
    res.json.should.have.been.calledWithExactly({message: 'health check failed: !'})
  })
})
