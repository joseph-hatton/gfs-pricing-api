module.exports = () => {
  const query = sinon.stub()
  const release = sinon.stub()
  return {
    pool: {
      connect: sinon.stub().resolves({
        query,
        release
      })
    },
    query,
    release
  }
}
