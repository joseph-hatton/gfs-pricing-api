const proxyquire = require('../../proxyquire')
const config = require('../../../api/config')

describe('db', () => {
  let db
  beforeEach(() => {
    class Pool {
      constructor (db) {
        this.db = db
        this.query = sinon.stub().returns('query result')
      }
    }
    db = proxyquire('api/db', {
      'pg': {
        'Pool': Pool
      }
    })
  })

  it('should return a pool craeted with the config', () => {
    db.pool.db.should.eql(config.db)
  })

  it('should return value from proxy pool\'s query', () => {
    db.query('text', {donkey: true}).should.eql('query result')
  })
})
