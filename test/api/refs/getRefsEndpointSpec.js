const mockResponse = require('../../mockResponse')
const getRefsEndpoint = require('../../../api/refs/getRefsEndpoint')
const accountingBases = require('../../../api/refs/accountingBases')
const dealStatuses = require('../../../api/refs/dealStatuses')
const urlRef = require('../../../api/refs/urlRef')
const probCloses = require('../../../api/refs/probCloses')
const productTypes = require('../../../api/refs/productTypes')
const stages = require('../../../api/refs/stages')
const typeBusinesses = require('../../../api/refs/typeBusinesses')
const previewValuesNames = require('../../../api/refs/previewValuesNames')
const businessUnitTree = require('../../../api/refs/businessUnitTree')
const uploadConfig = require('../../../api/refs/uploadConfig')
const uploadConfigLongevity = require('../../../api/refs/uploadConfigLongevity')
const countries = require('../../../api/refs/countries')
describe('getRefsEndpoint', () => {
  let res
  beforeEach(() => {
    res = mockResponse()
  })

  const apply = () => getRefsEndpoint({}, res)

  it('sends refs to json', async () => {
    await apply()

    res.json.should.have.been.calledWithExactly({
      accountingBases,
      dealStatuses,
      probCloses,
      productTypes,
      countries,
      previewValuesNames,
      stages,
      typeBusinesses,
      businessUnitTree,
      uploadConfig,
      uploadConfigLongevity,
      urlRef
    })
  })
})
