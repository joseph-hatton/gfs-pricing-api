const mockResponse = require('../../mockResponse')
const authorizeMiddleware = require('../../../api/auth/authorizeMiddleware')

describe('authorizeMiddleware', () => {
  let req, res, next

  const userId = 'user123'

  beforeEach(() => {
    req = {
      params: {userId},
      headers: {
        'user-id': userId
      }
    }
    res = mockResponse()
    next = sinon.stub()
  })

  const apply = () => authorizeMiddleware(req, res, next)

  const shouldBeAuthorized = () => {
    res.status.should.not.have.been.called
    next.should.have.been.calledWithExactly()
  }

  it('authorizes when current user matches user requested', () => {
    apply()

    shouldBeAuthorized()
  })

  it('authorizes when current user is not known', () => {
    req.headers = {}
    apply()

    shouldBeAuthorized()
  })

  it('sends 403 when current user does not match user', () => {
    const evilUser = 'satan123'
    req.headers['user-id'] = evilUser
    apply()

    next.should.not.have.been.called
    res.status.should.have.been.calledWithExactly(403)
    res.json.should.have.been.calledWithExactly({
      message: `user '${evilUser}' is not authorized to read or update user data for '${userId}'`
    })
  })
})
