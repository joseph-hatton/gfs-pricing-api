const entitlements = require('../../../api/auth')

describe('entitlements', () => {
  it('should have create deal', () => {
    should.exist(entitlements.CREATE_DEAL)
  })

  it('should have update deal', () => {
    should.exist(entitlements.UPDATE_DEAL)
  })

  it('should have delete deal', () => {
    should.exist(entitlements.DELETE_DEAL)
  })
})
