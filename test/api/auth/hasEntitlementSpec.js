const hasEntitlement = require('../../../api/auth/hasEntitlement')

describe('hasEntitlement', () => {
  it('should always return true currently', () => {
    hasEntitlement('some entitlement', 'userId').should.eql(true)
  })
})
