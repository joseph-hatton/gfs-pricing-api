module.exports = () => {
  const methods = [
    'info',
    'error'
  ]

  const mockLogs = {}

  methods.forEach(method => {
    mockLogs[method] = sinon.stub()
  })

  return mockLogs
}
